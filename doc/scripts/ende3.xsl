<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d"
                version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="yes"
            doctype-public="-//OASIS//DTD DocBook XML V5.0//EN"
            doctype-system="/usr/share/xml/docbook/schema/dtd/5.0/docbook.dtd"/>


<!-- ====================================================================== -->
<!-- Re-change remap attributes to xmi:id attributes to undo the according  -->
<!-- operation in ende1.xsl, see the comment there.                         -->

<xsl:template match="@remap">
  <xsl:attribute name="xml:id">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>


<!-- ====================================================================== -->
<!-- Default template: Just copy input to output                            -->

<xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>
