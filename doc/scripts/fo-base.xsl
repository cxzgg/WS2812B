<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="1.0">

<xsl:import href="/usr/share/xml/docbook/stylesheet/nwalsh5/current/fo/docbook.xsl"/>

<xsl:import href="base.xsl"/>


<!--FIXME:-->
<!--========================================================================-->
<!-- Keep graphical images and their captions together within one page      -->

<!--xsl:template match="d:mediaobject">
  <fo:block keep-together.within-column="always">
    <xsl:apply-imports/>
  </fo:block>
</xsl:template-->


<!--========================================================================-->
<!-- Parameters and attribute sets                                          -->

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- Global Document Parameters                                             -->

<xsl:param name="l10n.gentext.default.language">en</xsl:param>
<!--xsl:param name="l10n.gentext.default.language">de</xsl:param-->

<xsl:param name="draft.mode">no</xsl:param>

<xsl:param name="double.sided" select="0"/>

<xsl:param name="paper.type">A4</xsl:param>
<xsl:param name="page.margin.inner">22mm</xsl:param>
<xsl:param name="page.margin.outer">18mm</xsl:param>
<xsl:param name="body.start.indent">0pt</xsl:param>

<xsl:param name="symbol.font.family">Symbol</xsl:param><!-- No Zapfdingbats required -->
<xsl:param name="body.font.family">serif</xsl:param>
<!--xsl:param name="body.font.master">9.5</xsl:param--><!-- pt -->
<xsl:param name="line-height">1.5</xsl:param>
<xsl:param name="title.font.family" select="$body.font.family"/>

<!-- Redefine here to remove the symbol font reference -->
<xsl:param name="title.fontset" select="$title.font.family"/>

<xsl:param name="part.autolabel">0</xsl:param>

<xsl:param name="table.frame.border.thickness">1pt</xsl:param>
<xsl:param name="table.cell.border.thickness">1pt</xsl:param>

<!-- Move TOC inside the part to remove the part entry from the TOC -->
<xsl:param name="generate.toc">
book      nop
part      toc
</xsl:param>

<!--xsl:param name="toc.section.depth">3</xsl:param>
<xsl:param name="generate.section.toc.level" select="3"/-->

<xsl:param name="column.count.index">2</xsl:param>
<xsl:attribute-set name="index.page.number.properties">
  <xsl:attribute name="color">#0000FF</xsl:attribute>
</xsl:attribute-set>


<xsl:attribute-set name="normal.para.spacing">
  <xsl:attribute name="text-indent">0em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
  <xsl:attribute name="space-before.optimum">1.0em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
</xsl:attribute-set>


<xsl:param name="insert.link.page.number">yes</xsl:param>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- Page header and footer layout                                          -->

<xsl:param name="header.rule" select="0"/>
<xsl:param name="header.column.widths">1 6 1</xsl:param>
<xsl:attribute-set name="header.content.properties">
  <xsl:attribute name="font-size">
    <xsl:value-of select="$body.font.master * 1.3"/><xsl:text>pt</xsl:text>
  </xsl:attribute>
</xsl:attribute-set>


<!-- Header content so that not at TOC -->
<xsl:template name="header.content">
  <xsl:param name="pageclass" select="''"/>
  <xsl:param name="sequence" select="''"/>
  <xsl:param name="position" select="''"/>
  <xsl:param name="gentext-key" select="''"/>

  <fo:block>
    <!-- sequence can be odd, even, first, blank -->
    <!-- position can be left, center, right -->
    <xsl:if test="($sequence='odd' or $sequence='even')
              and $position='center'
              and $pageclass!='titlepage'
              and boolean(ancestor-or-self::d:chapter)">
      <xsl:apply-templates select="." mode="titleabbrev.markup"/>
    </xsl:if>
  </fo:block>
</xsl:template>

<xsl:param name="footer.rule" select="0"/>


<!--========================================================================-->
<!-- Undefine default message                                               -->
<!-- Default message should not be used because it pretends to print on     -->
<!-- US letter paper                                                        -->

<xsl:template name="root.messages"/>


<!--========================================================================-->
<!-- Use *language dependent* parameter Page-Ref-Template for the layout of -->
<!-- the page citations used in hyperlinks.                                 -->

<xsl:param name="Page-Ref-Template-en">, p.%p</xsl:param>
<xsl:param name="Page-Ref-Template-de">, S.%p</xsl:param>

<xsl:template match="*" mode="page.citation">
  <xsl:param name="id" select="'???'"/>
  <fo:basic-link internal-destination="{$id}"
                 xsl:use-attribute-sets="xref.properties">
    <fo:inline keep-together.within-line="always">
      <xsl:choose>
        <xsl:when test="ancestor::d:part/attribute::xml:lang='en'">
          <xsl:call-template name="substitute-markup">
            <xsl:with-param name="template" select="$Page-Ref-Template-en"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="ancestor::d:part/attribute::xml:lang='de'">
          <xsl:call-template name="substitute-markup">
            <xsl:with-param name="template" select="$Page-Ref-Template-de"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$l10n.gentext.default.language='de'">
          <xsl:call-template name="substitute-markup">
            <xsl:with-param name="template" select="$Page-Ref-Template-de"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="substitute-markup">
            <xsl:with-param name="template" select="$Page-Ref-Template-en"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </fo:inline>
  </fo:basic-link>
</xsl:template>


<!--========================================================================-->
<!-- Format of <indexentry> tags                                            -->

<xsl:template match="d:indexentry">
  <!-- FIXME: use-attribute-set -->
  <fo:block font-size="7.5pt"
            text-align="start"
            text-align-last="justify"
            text-indent="-1em"
            start-indent="1em">
    <xsl:apply-imports/>
  </fo:block>
</xsl:template>


<!--========================================================================-->
<!-- Link in index entries to determine destination page numbers and to     -->
<!-- get a proper index entry formatting.                                   -->

<xsl:template match="d:indexentry//d:link">
  <!-- Note: no whitespace allowed between those tags: -->
  <fo:basic-link internal-destination="{@linkend}"><xsl:apply-templates/><fo:leader leader-pattern="space"/><fo:page-number-citation ref-id="{@linkend}"/></fo:basic-link>
</xsl:template>


<!--========================================================================-->

<xsl:template match="processing-instruction('linebreak')">
  <fo:block/>
</xsl:template>


<!--========================================================================-->
<xsl:template name="debug">
<xsl:message>
  <xsl:text>XXXXXX</xsl:text>
  <!--xsl:value-of select="$xyz"/-->
</xsl:message>
</xsl:template>

</xsl:stylesheet>
