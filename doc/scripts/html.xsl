<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Copyright (C) 2018  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:import href="/usr/share/xml/docbook/stylesheet/nwalsh5/current/xhtml/docbook.xsl"/>
<!--xsl:import href="http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl"/-->

<xsl:import href="base.xsl"/>

<xsl:param name="l10n.gentext.default.language">en</xsl:param>
<!--xsl:param name="l10n.gentext.default.language">de</xsl:param-->

<xsl:param name="autotoc.label.in.hyperlink" select="0"/>

<xsl:param name="navig.graphics" select="0"/>

<xsl:param name="make.valid.html" select="1"/>


<xsl:param name="emphasis.propagates.style" select="1"/>
<xsl:param name="para.propagates.style" select="1"/>

<xsl:param name="html.stylesheet">style.css</xsl:param>
<xsl:param name="css.decoration">0</xsl:param>

<xsl:param name="part.autolabel">0</xsl:param>

<!-- If ignore.image.scaling is not set to 1 then all images are scaled to
     a fix width of nominal.image.width=540 pixel -->
<xsl:param name="ignore.image.scaling" select="1"/>

<!-- Move TOC inside the part to remove the part entry from the TOC -->
<xsl:param name="generate.toc">
book      nop
part      toc
</xsl:param>


<xsl:template name="part.titlepage.recto"/>
<xsl:template name="part.titlepage"/>


<!-- REMOVING UNDESIRED LANGUAGE TAGS

xsltproc (libxslt 10128) does not understand that:

<xsl:template match="*[@xml:lang!=$l10n.gentext.default.language]"/>


However also the following is not possible because xsltproc (libxslt 10128)
renoves undesired language tags _after_ creating the index so that it containes
undesired language entries:

<xsl:template match="*[@xml:lang]">
  <xsl:if test="@xml:lang=$l10n.gentext.default.language">
    <xsl:apply-imports/>
  </xsl:if>
</xsl:template>

So the part of removing undesired language tags had to be moved to an extra
xslt script rmlang.xsl.
-->

<xsl:template match="processing-instruction('linebreak')">
  <br/>
</xsl:template>

</xsl:stylesheet>
