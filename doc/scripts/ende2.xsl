<?xml version="1.0"?>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

Version 2   Added "|@endterm" to the match pattern at
            <xsl:template match="@remap|@linkend"> to cover also "endterm"
            attributes. Jost 2020-Oct-31

Copyright (C) 2018-2020  Jost Brachert, jost.brachert@gmx.de

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the license, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d"
                version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="yes"
            doctype-public="-//OASIS//DTD DocBook XML V5.0//EN"
            doctype-system="/usr/share/xml/docbook/schema/dtd/5.0/docbook.dtd"/>

<!--========================================================================-->
<!-- Remove those tags in <part> sections where the xml:lang attribute      -->
<!-- doesn't fit to the xml:lang attribute of the containing <part> tag.    -->

<xsl:template match="d:part//node()">
  <xsl:if test="not(@xml:lang) or @xml:lang=ancestor::d:part/attribute::xml:lang">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:if>
</xsl:template>


<!--========================================================================-->
<!-- Add a language dependent type attribute ("en" or "de") to <indexterm>  -->
<!-- tags.                                                                  -->
<!-- Get the language information from the xml:id attribute: If the ID is   -->
<!-- language dependent then the language mark is appended separated by     -->
<!-- an '_' ("_en" or "_de").                                               -->
<!-- If there is no language mark then the type attribute is taken from the -->
<!-- xml:lang attribute of an ancestor <part> tag.                          -->
<!-- The type attributes allows for separate indices for the languages.     -->
<!--                                                                        -->
<!-- Note that instead of the original xml:id attribute the remap attribute -->
<!-- must be used because in ende1.xsl all xml:id attributes have been      -->
<!-- replaced by remap, see the comment there.                              -->

<xsl:template match="d:part//d:indexterm">
  <indexterm>
    <xsl:choose>
      <xsl:when test="boolean(substring-before(@remap, '_en'))">
        <xsl:attribute name="type">
          <xsl:value-of select="'en'"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="boolean(substring-before(@remap, '_de'))">
        <xsl:attribute name="type">
          <xsl:value-of select="'de'"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="ancestor::d:part/attribute::xml:lang">
        <xsl:attribute name="type">
          <xsl:value-of select="ancestor::d:part/attribute::xml:lang"/>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>
    <xsl:for-each select = "@*">
      <xsl:call-template name="Reference"/>
    </xsl:for-each>
    <xsl:apply-templates/>
  </indexterm>
</xsl:template>


<!-- ====================================================================== -->
<!-- Add a language mark ("_en" or "_de") to id or reference attributes     -->
<!-- if there is yet no mark and if the language can be deteremined from    -->
<!-- an ancestor <part> tag's language attribute.                           -->
<!--                                                                        -->
<!-- Note that instead of the original xml:id attribute the remap attribute -->
<!-- must be used because in ende1.xsl all xml:id attributes have been      -->
<!-- replaced by remap, see the comment there.                              -->


<xsl:template match="@remap|@linkend|@endterm">
  <xsl:call-template name="Reference"/>
</xsl:template>


<!-- ====================================================================== -->
<!-- Add a language mark ("_en" or "_de") to id or reference attributes     -->
<!-- if there is yet no mark and if the language can be determined from     -->
<!-- an ancestor <part> tag's language attribute.                           -->
<!--                                                                        -->
<!-- To be called for an attribute.                                         -->
<!--                                                                        -->
<!-- If the value of the attribute does not end in '_en' or '_de' and if    -->
<!-- the ancestor <part> tag has an xml:lang attribute then                 -->
<!--   append the xml:lang attribute of the ancestor <part> tag to the      -->
<!--   attribute value separated by an '_'.                                 -->
<!-- otherwise                                                              -->
<!--   leave the attribute value as it is.                                  -->
<!--                                                                        -->
<!-- CAUTION: works as intended only if '_en' or '_de' do not accidentally  -->
<!--          occur as part of the attribute in the middle of it.           -->

<xsl:template name="Reference">
  <xsl:attribute name="{name(.)}">
    <xsl:choose>
      <xsl:when test="not(substring-before(., '_en'))
                  and not(substring-before(., '_de'))
                  and ancestor::d:part/attribute::xml:lang">
        <xsl:value-of select="concat(.,'_',ancestor::d:part/attribute::xml:lang)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>


<!-- ====================================================================== -->
<!-- Add a type attribute to an <index> tag depending on an ancestor <part> -->
<!-- tag's language attribute in case there is one.                         -->

<xsl:template match="d:part//d:index">
  <xsl:variable name="lang" select="ancestor::d:part/attribute::xml:lang"/>
  <index>
    <xsl:if test="$lang">
      <xsl:attribute name="type">
        <xsl:value-of select="$lang"/>
      </xsl:attribute>
    </xsl:if>
  </index>
</xsl:template>


<!-- ====================================================================== -->
<!-- Default template: Just copy input to output                            -->

<xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

</xsl:stylesheet>
