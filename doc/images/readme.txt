The SVG images are either created manually using Inkscape or created by Fritzing
and modified manually using Inkscape.

The images contain manually inserted embedded fonts to prevent from external
font references in the resulting PDF etc. files.

Those embedded fonts have been optimized in the respective image using Perl
script scripts/optEmbSvgFont.pl to reduce the size of the images.
