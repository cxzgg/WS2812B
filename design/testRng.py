#!/usr/bin/env python3
#
# testRng.py
#
# Testscript to test a Linear Congruential Pseudo Random Number Generator
#
# Version 1    2022 Sep 10th   jost.brachert@gmx.de
#
#-------------------------------------------------------------------------------
# Copyright (C) 2022  jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.


#*******************************************************************************
# Linear Congruential Pseudo Random Number Generator (LCG)
#
# On every call calculates another 8 bit random number
# with a Linear Congruential Generator with a state of M bit size
# and provides it as return value.
#
# x{n+1} = (a*x{n} + c) % m       n=0,1,...
# r{n}   = x{n} / w
#
# a, c, x integer numbers from interval [0,m).
# The generator is called a "mixed" generator if c!=0.
#
# The size of the period is m if:
# The period of the LCG is maximal (i.e. equal m) if
# a) c != 0,
# b) m and c are relatively prime,
# c) a - 1 is divisible by all prime factors of m,
# d) a - 1 is divisible by 4 if m is divisible by 4.
#
# regarding periods of the bits of the internal state:
# 1) If a = K*2**s+1 (K odd, s integer, s>=2) then bit d(j) of x{n}, j=0...m-1,
#    the period 2**(j+1).
# 2) Else if a = K*2**s-1 then
#    - bits d(0) have a period of 2, that means they alternate between 0 and 1,
#    - bits d(j), j=1...s-1 have a period of 1 (always the same value) or 2,
#      depending on x{0},
#    - bits d(j), j=s...m-1 have a period of 2**(j+2-s).
#

class lcg16_8:
    FACT     = 64747
    ADDCONST =     1
    M        =    16        # Bitwidth of the state, M = 2**m
    state    =     0        # LCG internal state with its initial value

    def __init__(self, stateInit=0):
        self.state = stateInit

    def get (self):
        self.state = (self.state * self.FACT + self.ADDCONST) & ((1<<self.M)-1)
        return (self.state >> 4) & 0xFF


t = [0]*256                     # Array for test of uniformity of distribution

rng = lcg16_8()                 # Create the generator

n = 1024
print ("Random sequence 1...%d:", n)
for i in range (n):
    r = rng.get()
    t[r] += 1
    print ("%3d: %3d %10s" % (i, r, bin(r)))


print ("Uniformity test (1...%d:", n)
for i in range (256):
    if i%10 == 0:  print ("%3d:" % (i), end="")
    print (" %3d" % (t[i]), end="")
    if i%10 == 10-1:  print ()

print ()

n1 = 8192
n = n1 - n
print ("Uniformity test (1...%d:", n1)
for i in range (n):
    r = rng.get()
    t[r] += 1

for i in range (256):
    if i%10 == 0:  print ("%3d:" % (i), end="")
    print (" %3d" % (t[i]), end="")
    if i%10 == 10-1:  print ()

print ()

