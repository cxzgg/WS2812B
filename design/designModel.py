#!/usr/bin/env python3
#
# Design model to test certain functions for the WS2812B Driving Unit before
# taking them over to the embedded controller.
#
# For details see the design document AnaReqDes.txt or AnaReqDes.html.
#
# Version 1    2022 Sep 10th   jost.brachert@gmx.de
#
#-------------------------------------------------------------------------------
# Copyright (C) 2022  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.


import tkinter as tk


deltaT = 25                 # Sampling period [ms], time between time increments


#*******************************************************************************
# PatternDef
#
# Define a color pattern
#

class PatternDef:
    table = []
    colorCircleInpol = 0    # =1: Interpolate between colors in the HBS domain
                            # =0: Interpolate between colors in the RGB domain

    # nCircle: See design issue [FuDsMountOnBicycle]
    def __init__(self, nCircle, nLeds, tableInit, colorCircleInpolInit=0):
        self.table = tableInit

        # Last segment gets the remaining number of LEDs
        self.table[-1]['n'] = nCircle
        for j in range( len(self.table) - 1 ):
            self.table[-1]['n'] -= self.table[j]['n']

        self.colorCircleInpol = colorCircleInpolInit

    #---------------------------------------------------------------------------
    # setLed
    #
    # Set an RGB color value at the LED pointed to by argument led.
    # The color is put to the LED by addition so that multiple calls to setLed()
    # for the same LED add up to a final color value.
    # On overflow the color channel values are limited to 255..

    def setLed (self, led, red, green, blue):
        led[0] = min( led[0]+red  , 255 )
        led[1] = min( led[1]+green, 255 )
        led[2] = min( led[2]+blue , 255 )

    #---------------------------------------------------------------------------
    # setPatternLeds
    #
    # Put a pattern defined by self.table[] to the message array led[] to be
    # later put out to the WS2812B LEDs.
    #
    # The pattern is put to the array by addition so that several calls to
    # setPatternLeds() will superpose the patterns.
    #
    # leds[]        : Array of LED color values later put out to the WS2812B bus
    # nLeds         : Number of LEDs in leds[]
    # nCircle       : See design issue [FuDsMountOnBicycle]
    # offset        : Shifts the pattern along the LED chain, may be negative
    # width         : The segment size can be reduced to width/256u.
    #                 In that case loop counter i doesn't reach the loop end
    #                 nCircle after all segments are processed once.
    #                 For that case has the outer loop for(;;){..} been added.
    #                 That loop starts over until i reaches nCircle and iLED
    #                 reaches nLeds.
    #                 The pattern is thus compressed and manifold replicated.
    # brightnessMod : Modulation of brightness
    # locationMod   : Modulation of the location of the the pattern origin

    def setLeds (self, leds, nLeds, nCircle, offset=0, brightnessMod=256, locationMod=256):

        # Actually: offset %= nCircle
        while offset >= nCircle:  offset -= nCircle
        while offset <     0   :  offset += nCircle

        i = 0
        while 1:
            for cj1 in range (len(self.table)): # cj1: Index of 1st color
                # Number of LEDs in the segment to be interpolated
                dj = int( self.table[cj1]['n']*locationMod/256 )
                if dj == 0:  dj = 1

                cj2 = cj1+1                     # Index of 2nd color
                if cj2 == len(self.table):  cj2 = 0

                c1 = self.table[cj1]['col']     # 1st color, start of interpolation
                c2 = self.table[cj2]['col']     # 2nd color, end of interpolation

                for j in range (dj):
                    if self.colorCircleInpol:
                        # Interpolation between the colors c1 and c2
                        # along the chromatic circle
                        hue1, brightness1, saturation1 = rgb2hbs (c1[0],c1[1],c1[2])
                        hue2, brightness2, saturation2 = rgb2hbs (c2[0],c2[1],c2[2])

#                        if app.time==0:
#                            print ("%3d-%3d: %3d %3d %3d | %3d %3d %3d | %3d %3d %3d"
#                                   % (i,j, c1[0],c1[1],c1[2],
#                                      hue1,brightness1,saturation1,
#                                      hue2,brightness2,saturation2))

                        hue        = hue1       +int((hue2       -hue1       )*j/dj)
                        brightness = brightness1+int((brightness2-brightness1)*j/dj)
                        saturation = saturation1+int((saturation2-saturation1)*j/dj)

#                        if app.time==0:
#                            print ("%3d~%3d: %3d %3d %3d | %3d %3d %3d | %3d %3d %3d | %3d %3d %3d"
#                                   % (i,j, c1[0],c1[1],c1[2],
#                                      hue ,brightness ,saturation ,
#                                      hue1,brightness1,saturation1,
#                                      hue2,brightness2,saturation2))

                        red, green, blue = hbs2rgb (hue, brightness, saturation)
                    else:
                        # Interpolation between the colors c1 and c2
                        red   = c1[0] + int((c2[0]-c1[0])*j/dj)
                        green = c1[1] + int((c2[1]-c1[1])*j/dj)
                        blue  = c1[2] + int((c2[2]-c1[2])*j/dj)

#                    if app.time==0:
#                        print ("%3d~%3d: %3d %3d %3d" % (i, j, red, green, blue))

                    red   = int( red   * brightnessMod / 256 )
                    green = int( green * brightnessMod / 256 )
                    blue  = int( blue  * brightnessMod / 256 )

                    iLed = i + offset

#                    if app.time==0:
#                        print ("%3d~%3d: %3d %3d %3d" % (i, iLed, red, green, blue))

                    if i < nCircle:
                        iLed1 = iLed
                        # Actually: iLed1 %= nCircle
                        if iLed1 >= nCircle:  iLed1 -= nCircle
                        self.setLed (leds[iLed1], red, green, blue)

                    if iLed >= nCircle and iLed < nLeds:
                        self.setLed (leds[iLed], red, green, blue)

                    i += 1

                    if iLed >= nLeds and i >= nCircle:  break

                if iLed >= nLeds and i >= nCircle:  break

            if iLed >= nLeds and i >= nCircle:  break



#*******************************************************************************
# ModDef
#
# Define a Modulation Characteristic
#
# A modulation is a characteristic consisting of modulation value nodes and the
# duration between those nodes. It is organized as a drum storage. That means
# after its last entry it wraps around to its first entry.
#

class ModDef:
    table = []    # Array of dictionaries, each with a mod. value and a duration
    time1 = 0     # Time of current node [ms]
    time2 = 0     # Time for the next node [ms]
    idx   = 0     # Index of the cuttent node
    timeFactor  = 255       # Timing modulation factor
                            # 255=100%, values<255 shorten the duration
    valueFactor = 255

    def __init__(self, tableInit):
        self.table = tableInit
        self.time2 = self.table[0]['t']

    # Interpolate between the current nodes

    def getValue (self, time):
        if time >= self.time2:
            self.idx += 1
            if self.idx >= len(self.table):  self.idx = 0

#            self.time1 = self.time2
            self.time1 = time
            # Duration after modulation by timeFactor
            dtnew = int( self.table[self.idx]['t']*self.timeFactor/255 )
            if (dtnew < 1):  dtnew = 1
#            self.time2 = self.time1 + dtnew
            self.time2 = time + dtnew

        idx2 = self.idx + 1
        if idx2 >= len(self.table):  idx2 = 0

        val1 = int( self.table[self.idx ]['val']*self.valueFactor/255 )
        val2 = int( self.table[     idx2]['val']*self.valueFactor/255 )

        dt2 = self.time2 - self.time1
        dt = time - self.time1

#        if dt > dt2:  dt = dt2

#        if time >= self.time2:
#            self.idx += 1
#            if self.idx >= len(self.table):  self.idx = 0
#
##            self.time1 = self.time2
#            self.time1 = time
#            dtnew = int( self.table[self.idx]['t']*self.timeFactor/255 )
#            if (dtnew < 1):  dtnew = 1
##            self.time2 = self.time1 + dtnew
#            self.time2 = time + dtnew

        return val1 + int((val2-val1)*dt / dt2)

    def modTime (self, factor):
        self.timeFactor = factor

    def modValue (self, factor):
        self.valueFactor = factor



#*******************************************************************************
# rgb2hbs
#
# Convert a color given by its red, green, blue (RGB) values to hue, brightness,
# saturation (HBS) specification.
#

def rgb2hbs (r, g, b):
    brightness = max(r,g,b)
    if brightness == 0:
        saturation = 0
        hue = 0
    else:
        r = int( r*255/brightness )
        g = int( g*255/brightness )
        b = int( b*255/brightness )
        cmin = min(r,g,b)
        saturation = max(255-r, 255-g, 255-b)
        if saturation == 0:
            hue = 0
        else:
            r = int( (r-cmin)*255/saturation )
            g = int( (g-cmin)*255/saturation )
            b = int( (b-cmin)*255/saturation )
            if r == 255:
                if b == 0:  hue = g
                else     :  hue = 6*255 - b
            elif g == 255:
                if b == 0:  hue = 2*255 - r
                else     :  hue = 2*255 + b
            else: # b == 255
                if g == 0:  hue = 4*255 + r
                else     :  hue = 4*255 - g

    return (hue, brightness, saturation)



#*******************************************************************************
# hbs2rgb
#
# Convert a color given by its hue, brightness, saturation (HBS) values to
# red, green, blue (RGB) specification.
#

def hbs2rgb (hue, brightness, saturation):
    # hue: 0...6*255-1=1529, brightness: 0...255, saturation: 0...255
    # Calculate colors from hue value
    if hue >= 0 and hue < 255:
        r = 255
        g = hue
        b = 0
    elif hue >= 255 and hue < 2*255:
        r = 2*255 - hue
        g = 255
        b = 0
    elif hue >= 2*255 and hue < 3*255:
        r = 0
        g = 255
        b = hue - 2*255
    elif hue >= 3*255 and hue < 4*255:
        r = 0
        g = 4*255 - hue
        b = 255
    elif hue >= 4*255 and hue < 5*255:
        r = hue - 4*255
        g = 0
        b = 255
    else: # hue >= 5*255 and hue < 6*255
        r = 255
        g = 0
        b = 6*255 - hue

    # Saturation:
    #    saturation = 255: colors unchanged
    #    saturation =   0: all colors = 255 ("white")
    r = 255 - int( (255-r)*saturation/255 )
    g = 255 - int( (255-g)*saturation/255 )
    b = 255 - int( (255-b)*saturation/255 )

    # Brightness
    r = int( r * brightness / 255 )
    g = int( g * brightness / 255 )
    b = int( b * brightness / 255 )

    return (r,g,b)



#*****************************************************************************
# Linear Congruential Random Number Generator
#
# On every call calculates another 8 bit random number
# with a Linear Congruential Generator with a state of 16 bit size
# and provides it as return value.
#
# For details see design issues [FuDsRNG], [FuDsRngConstants], [FuDsBitPeriods],
#

class lcg16_8:
    FACT     = 64747
    ADDCONST = 1
    state    = 0

    def __init__(self, stateInit=0):
        self.state = stateInit

    def get (self):
        self.state = (self.state * self.FACT + self.ADDCONST) & 0xFFFF
        return (self.state >> 4) & 0xFF



#*****************************************************************************
# Main Application Class
#

class Application (tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid ()
        self.createWidgets ()
        print ("Press shift-q to quit")

    nLeds    = 300          # Number of LEDs, i.e. number of entries in leds[]
    nCircle  = 200          # See design issue [FuDsMountOnBicycle]
    testBtn  = 0            # Initial state of test button
    pauseBtn = 0            # Initial state of pause button
    time     = 0            # Current time in ms

    #===========================================================================
    # Define the GUI

    def createWidgets (self):
        ledHeight = 10
        ledWidth  =  3
        self.ledLine = tk.Canvas (self, width=ledWidth*self.nLeds, height=ledHeight)
        self.leds = []
        # Note: the values returned by the create_rectangle constructors are IDs
        # as integer numbers counting from 1 onwards
        for i in range(self.nLeds):
            fill = "#0C0"
            if i%30 == 0:  fill = "white"
            x1 = (i  )*ledWidth
            x2 = (i+1)*ledWidth
            y2 = 10
            self.leds.append (self.ledLine.create_rectangle (x1, 0, x2, y2, fill=fill))

        self.ledLine.grid (row=0,column=0,sticky=tk.N+tk.W)

        self.pauseButton = tk.Button (self, text='Pause')
        self.pauseButton.grid (row=1, column=0, sticky=tk.N+tk.W)
        self.pauseButton.bind ("<Button-1>", self.pauseButtonClick)

        self.testButton = tk.Button (self, text='Test')
        self.testButton.grid (row=1, column=1, sticky=tk.N+tk.W)
        self.testButton.bind ("<Button-2>", self.testButtonClick)

        self.quitButton = tk.Button (self, text='Quit', command=self.quit)
        self.quitButton.grid (row=2, column=0, sticky=tk.N+tk.W)

        self.bind_all ('<KeyPress-Q>',self.quitShortcutEventHandler)

        self.initAlgo ()

        self.after (1000, self.loop)


    def testButtonClick (self, event):
        self.testBtn ^= 1
        print ("Test button", self.testBtn)


    def pauseButtonClick (self, event):
        self.pauseBtn ^= 1
        print ("Pause button", self.pauseBtn)


    def quitShortcutEventHandler (self, event):
        print ("Quit Shortcut")
        self.quit ()


    #===========================================================================
    # Initialize the application

    def initAlgo (self):
        # Define nodes of the modulation characteristics
        # ModDef ([{ 'val':mod.value, 't':duration}, ...])
        #   mod.value: modulation value of the node [0...255]
        #   duration : time until the next node     [ms]
        self.briMod = ModDef ([
#            { 'val':255, 't':1000},
#            { 'val':  0, 't':  50},

#            { 'val':255, 't':100},
#            { 'val':  0, 't':100},

            { 'val':255, 't':2000},
            { 'val':  0, 't':2000},

#            { 'val':150, 't':2000},
#            { 'val':150, 't':   1},
        ])

        self.ofsMod = ModDef ([
#            { 'val':   0, 't':2000},
##            { 'val':-self.nCircle-int(self.nCircle*deltaT/2000), 't':   1},
#            { 'val': self.nCircle-int(self.nCircle*deltaT/2000), 't':   1},

            { 'val':   0, 't':1000},
            { 'val': self.nCircle-int(self.nCircle*deltaT/1000), 't':  1},
        ])

        self.ofsTMod = ModDef ([
            { 'val':255, 't':5000},
            { 'val':512, 't':   0},
        ])

        self.mod4 = ModDef ([
            { 'val':255, 't':5000},
            { 'val':  0, 't':   0},
        ])

        self.pattern = [
            PatternDef (self.nCircle, self.nLeds, [
                # 'n' _must not_ be 0, except for the last element, which _must_ be zero
                { 'col':[255,  0,  0], 'n':int(self.nCircle/4) },
                { 'col':[  0,255,  0], 'n':int(self.nCircle/4) },
                { 'col':[128, 64,255], 'n':0 },

#                { 'col':[255,  0,  0], 'n':1 },
#                { 'col':[  0,  0,  0], 'n':1 },
#                { 'col':[255,  0,  0], 'n':int(self.nCircle/4) },
#                { 'col':[  0,255,  0], 'n':1 },
#                { 'col':[  0,  0,  0], 'n':1 },
#                { 'col':[  0,255,  0], 'n':int(self.nCircle/4) },
#                { 'col':[128, 64,255], 'n':1 },
#                { 'col':[  0,  0,  0], 'n':1 },
#                { 'col':[128, 64,255], 'n':0 },

# One 10-LED spot
#                { 'col':[  0,  0,  0], 'n':1  },
#                { 'col':[255,  0,  0], 'n':10 },
#                { 'col':[255,  0,  0], 'n':1  },
#                { 'col':[  0,  0,  0], 'n':0  },

# Three 10-LED spots
#                { 'col':[  0,  0,  0], 'n':1  },
#                { 'col':[255,  0,  0], 'n':10 },
#                { 'col':[255,  0,  0], 'n':1  },
#                { 'col':[  0,  0,  0], 'n':int((self.nCircle-3*(1+10+1))/3)},
#                { 'col':[  0,  0,  0], 'n':1  },
#                { 'col':[255,  0,  0], 'n':10 },
#                { 'col':[255,  0,  0], 'n':1  },
#                { 'col':[  0,  0,  0], 'n':int((self.nCircle-3*(1+10+1))/3)},
#                { 'col':[  0,  0,  0], 'n':1  },
#                { 'col':[255,  0,  0], 'n':10 },
#                { 'col':[255,  0,  0], 'n':1  },
#                { 'col':[  0,  0,  0], 'n':0  },


#                { 'col':[255,128,  0], 'n':int(self.nCircle/3) },
#                { 'col':[255, 64,  0], 'n':int(self.nCircle/5) },
#                { 'col':[255,255,  0], 'n':int(self.nCircle/5) },
#                { 'col':[255,128,  0], 'n':int(self.nCircle/5) },
#                { 'col':[192,128,  0], 'n':0 },
            ], 1),
            PatternDef (self.nCircle, self.nLeds, [
                # 'n' _must not_ be 0, except for the last element, which _must_ be zero
                { 'col':[  0,  0,  0], 'n':1  },
                { 'col':[  0,255,255], 'n':10 },
                { 'col':[  0,255,255], 'n':1  },
                { 'col':[  0,  0,  0], 'n':0  },
            ]),
        ]


    #===========================================================================
    # Application part of the main loop

    def loop (self):
        if (not self.pauseBtn):
            brightnessMod = self.briMod .getValue (self.time)
            offsetTimeMod = self.ofsTMod.getValue (self.time)
#            offsetValueMod= self.mod4.getValue (self.time)
#            self.ofsMod.modTime (offsetTimeMod)
            offset        = self.ofsMod .getValue (self.time)

            leds = [3*[0] for x in range(self.nLeds)]

#            self.pattern[0].setLeds (leds, self.nLeds, self.nCircle, 0, 256)
            self.pattern[0].setLeds (leds, self.nLeds, self.nCircle, offset, 256)
#            self.pattern[0].setLeds (leds, self.nLeds, self.nCircle, 0, brightnessMod)
#            self.pattern[0].setLeds (leds, self.nLeds, self.nCircle, offset, brightnessMod)

            for iLed in range(self.nLeds):
                color = "#%02x%02x%02x" % (leds[iLed][0], leds[iLed][1], leds[iLed][2])
                self.ledLine.itemconfigure (self.leds[iLed],fill=color)

#            if (self.time % 100 == 0):  print ("OT:%4d O:%3d" % (offsetTimeMod,self.ofsMod.time2))
            if (self.time % 1000 == 0):  print ("Time:", self.time)
            self.time += deltaT

        self.after (deltaT, self.loop)


################################################################################
# Main Program

app = Application ()
app.master.title ('WS2812B Linear Test')
app.mainloop ()
