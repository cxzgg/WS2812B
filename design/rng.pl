#!/usr/bin/perl -w
#
# Check all prime numbers between 3 and 0xFFFF regarding certain properties
# if used in a linear congruential pseudo random number genetaor.
#
# For details see design issue [FuDsBitPeriods] in AnaReqDes.txt or
# AnaReqDes.html.
#
# Version 1    2022 Sep 10th   jost.brachert@gmx.de
#
#-------------------------------------------------------------------------------
# Copyright (C) 2022  jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.

use strict;

#-------------------------------------------------------------------------------
# Calculate prime numbers until 0xFFFF

print "Prime Number n   (n-1)\%4   n = K1*2**s+1 or K2*2**s-1\n";
for (my $n=3; $n<=0xFFFF; $n+=2) {
    my $prime = 1;
    for (my $f=3; $f<=int(sqrt($n)); $f+=2) {
        if ($n % $f == 0) {
            # $f is a factor of $n
            $prime = 0;
            last;
        }
    }
    if ($prime) {
        printf "%5d:             ", $n;

        # Check whether n is divisable by 4
        printf "%s     ", ($n-1)%4 == 0 ? 'F' : 'T';

        # Check whether n = K1*2**s+1, K1 odd, s>=2
        my @KS = K1($n);
        foreach my $ks (@KS) {
            printf "    K1:%5d, s:%2d", $$ks{K}, $$ks{s};
        }
        # Check whether n = K2*2**s+1, K2 odd, s>=2
        @KS = K2($n);
        foreach my $ks (@KS) {
            printf "    K2:%5d, s:%2d", $$ks{K}, $$ks{s};
        }
        print "\n";
    }
}



# Check whether argument is equal K*2**s+1, K odd, s>=2

sub K1 {
    my $a = $_[0];
    my @KS;
    for (my $s=2; $s<16; ++$s) {
        for (my $K=1; ; $K+=2) {
            my $v = $K*2**$s + 1;
            if ($v == $a) {
                $KS[@KS] = {K=>$K, s=>$s};
            }
            last  if $v >= $a;
        }
    }
    return @KS;
}



# Check whether argument is equal K*2**s-1, K odd, s>=2

sub K2 {
    my $a = $_[0];
    my @KS;
    for (my $s=2; $s<16; ++$s) {
        for (my $K=1; ; $K+=2) {
            my $v = $K*2**$s - 1;
            if ($v == $a) {
                $KS[@KS] = {K=>$K, s=>$s};
            }
            last  if $v >= $a;
        }
    }
    return @KS;
}