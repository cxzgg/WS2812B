//##############################################################################
//
// preproc-test.cpp
//
// Program to test the design of the calculation of the number of NOPs to
// achieve a correct timing of the signal to the WS2812B LEDs
//
// Compile with
//   gcc -DF_CPU=<CPUfrequency> preproc-test.cpp -o preproc-test
// e.g.
//   gcc -DF_CPU=20000000 preproc-test.cpp -o preproc-test
//
// For details see the design document AnaReqDes.txt or AnaReqDes.html
//
// Version 1    2022 Sep 10th   jost.brachert@gmx.de
//
//------------------------------------------------------------------------------
// Copyright (C) 2022  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the license, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//

#include <stdio.h>

#ifndef F_CPU
#error F_CPU not defined
#endif

#ifdef ACTIVE_FOR_FINAL_CODE
#if F_CPU < 12000000
#error F_CPU too low. F_CPU must be >= 12 MHz
#endif

#if F_CPU > 20000000
#error F_CPU too high. F_CPU must be <= 20 MHz
#endif
#endif


//==============================================================================
// Calculate the NOPs (no-operation statements) in a piece of assembler code to
// achieve the required WS2812B timing
//
// WS2812B specification on the timing to put out one bit:
//
// "0":_-----___________-   "1":_---------_____-
//      <T0H><---T0L--->         <--T1H--><T1L>
//           <T01L><T1L>
//
// T01L = T0L - T1L

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Nominal times for the periods of the WS2812B protocol
// See design document section "Selecting Nominal Protocol Values"

#define  T1H_NOM   1000  // Nominal time for T1H in ns
#define  T1L_NOM    380  // Nominal time for T1L in ns
#define  T0H_NOM    380  // Nominal time for T0H in ns
#define  T0L_NOM   1000  // Nominal time for T0L in ns

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Number of assembly instruction CPU cycles (beside NOPs)
// See design issue [SwDsOneBitOut]

#define  NC1H         5  // No. of asm instr. CPU cyles (beside NOPs) for T1H
#define  NC1L         3  // No. of asm instr. CPU cyles (beside NOPs) for T1L
#define  NC0H         6  // No. of asm instr. CPU cyles (beside NOPs) for T0H
#define  NC0L         1  // No. of asm instr. CPU cyles (beside NOPs) for T0L

//------------------------------------------------------------------------------
// Templates to place a certain number of NOPS in assembler code

#define  NOPS_0     ""
#define  NOPS_1     NOPS_0  "nop\n\t"
#define  NOPS_2     NOPS_1  "nop\n\t"
#define  NOPS_3     NOPS_2  "nop\n\t"
#define  NOPS_4     NOPS_3  "nop\n\t"
#define  NOPS_5     NOPS_4  "nop\n\t"
#define  NOPS_6     NOPS_5  "nop\n\t"
#define  NOPS_7     NOPS_6  "nop\n\t"
#define  NOPS_8     NOPS_7  "nop\n\t"
#define  NOPS_9     NOPS_8  "nop\n\t"
#define  NOPS_10    NOPS_9  "nop\n\t"
#define  NOPS_11    NOPS_10 "nop\n\t"
#define  NOPS_12    NOPS_11 "nop\n\t"
#define  NOPS_13    NOPS_12 "nop\n\t"
#define  NOPS_14    NOPS_13 "nop\n\t"
#define  NOPS_15    NOPS_14 "nop\n\t"
#define  NOPS_16    NOPS_15 "nop\n\t"
#define  NOPS_17    NOPS_16 "nop\n\t"
#define  NOPS_18    NOPS_17 "nop\n\t"
#define  NOPS_19    NOPS_18 "nop\n\t"
#define  NOPS_20    NOPS_19 "nop\n\t"
#define  NOPS_21    NOPS_20 "nop\n\t"
#define  NOPS_22    NOPS_21 "nop\n\t"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// At first calculate the number of NOPs for the short periods T1L and T0H

// Number of NOPS to realize T1L
#define  N_NOPS_T1L  ( ((T1L_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC1L) >=0 ?\
                       ((T1L_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC1L) : 0 )
// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T1L < 1
#  define  NOPS_T1L  NOPS_0
#elif    N_NOPS_T1L < 2
#  define  NOPS_T1L  NOPS_1
#elif    N_NOPS_T1L < 3
#  define  NOPS_T1L  NOPS_2
#elif    N_NOPS_T1L < 4
#  define  NOPS_T1L  NOPS_3
#elif    N_NOPS_T1L < 5
#  define  NOPS_T1L  NOPS_4
#elif    N_NOPS_T1L < 6
#  define  NOPS_T1L  NOPS_5
#elif    N_NOPS_T1L < 7
#  define  NOPS_T1L  NOPS_6
#elif    N_NOPS_T1L < 8
#  define  NOPS_T1L  NOPS_7
#elif    N_NOPS_T1L < 9
#  define  NOPS_T1L  NOPS_8
#elif    N_NOPS_T1L < 10
#  define  NOPS_T1L  NOPS_9
#else
#  error no valid NOPS_T1L
#endif

// Number of NOPS to realize T0H
#define  N_NOPS_T0H  ( ((T0H_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC0H) >=0 ?\
                       ((T0H_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC0H) : 0 )
// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T0H < 1
#  define  NOPS_T0H  NOPS_0
#elif    N_NOPS_T0H < 2
#  define  NOPS_T0H  NOPS_1
#elif    N_NOPS_T0H < 3
#  define  NOPS_T0H  NOPS_2
#elif    N_NOPS_T0H < 4
#  define  NOPS_T0H  NOPS_3
#elif    N_NOPS_T0H < 5
#  define  NOPS_T0H  NOPS_4
#elif    N_NOPS_T0H < 6
#  define  NOPS_T0H  NOPS_5
#elif    N_NOPS_T0H < 7
#  define  NOPS_T0H  NOPS_6
#elif    N_NOPS_T0H < 8
#  define  NOPS_T0H  NOPS_7
#elif    N_NOPS_T0H < 9
#  define  NOPS_T0H  NOPS_8
#elif    N_NOPS_T0H < 10
#  define  NOPS_T0H  NOPS_9
#else
#  error no valid NOPS_T0H
#endif

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Calculate the number of NOPs of the long periods T0L and T1H

// Required number of total assembler instructions to achieve the required freq.
#define  N_INSTR_TOTAL  ((( (T0H_NOM)+(T0L_NOM) )*(((F_CPU)+500)/1000)+500000)/1000000)

// Number of NOPS to realize T1H
#define  N_NOPS_T1H     ((N_INSTR_TOTAL) - (N_NOPS_T1L) - (NC1L) - (NC1H))

// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T1H < 6
#  error no valid NOPS_T1H
#elif    N_NOPS_T1H < 7
#  define  NOPS_T1H  NOPS_6
#elif    N_NOPS_T1H < 8
#  define  NOPS_T1H  NOPS_7
#elif    N_NOPS_T1H < 9
#  define  NOPS_T1H  NOPS_8
#elif    N_NOPS_T1H < 10
#  define  NOPS_T1H  NOPS_9
#elif    N_NOPS_T1H < 11
#  define  NOPS_T1H  NOPS_10
#elif    N_NOPS_T1H < 12
#  define  NOPS_T1H  NOPS_11
#elif    N_NOPS_T1H < 13
#  define  NOPS_T1H  NOPS_12
#elif    N_NOPS_T1H < 14
#  define  NOPS_T1H  NOPS_13
#elif    N_NOPS_T1H < 15
#  define  NOPS_T1H  NOPS_14
#elif    N_NOPS_T1H < 16
#  define  NOPS_T1H  NOPS_15
#elif    N_NOPS_T1H < 17
#  define  NOPS_T1H  NOPS_16
#elif    N_NOPS_T1H < 18
#  define  NOPS_T1H  NOPS_17
#elif    N_NOPS_T1H < 19
#  define  NOPS_T1H  NOPS_18
#elif    N_NOPS_T1H < 20
#  define  NOPS_T1H  NOPS_19
#elif    N_NOPS_T1H < 21
#  define  NOPS_T1H  NOPS_20
#elif    N_NOPS_T1H < 22
#  define  NOPS_T1H  NOPS_21
#else
#  error no valid NOPS_T1H
#endif

// Number of NOPS to realize T01L=T0L-T1L
#define  N_NOPS_T01L    ((N_INSTR_TOTAL)-(N_NOPS_T0H)-(NC0H)-(NC0L)-(N_NOPS_T1L))

// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T01L < 1
#  define  NOPS_T01L  NOPS_0
#elif    N_NOPS_T01L < 2
#  define  NOPS_T01L  NOPS_1
#elif    N_NOPS_T01L < 3
#  define  NOPS_T01L  NOPS_2
#elif    N_NOPS_T01L < 4
#  define  NOPS_T01L  NOPS_3
#elif    N_NOPS_T01L < 5
#  define  NOPS_T01L  NOPS_4
#elif    N_NOPS_T01L < 6
#  define  NOPS_T01L  NOPS_5
#elif    N_NOPS_T01L < 7
#  define  NOPS_T01L  NOPS_6
#elif    N_NOPS_T01L < 8
#  define  NOPS_T01L  NOPS_7
#elif    N_NOPS_T01L < 9
#  define  NOPS_T01L  NOPS_8
#elif    N_NOPS_T01L < 10
#  define  NOPS_T01L  NOPS_9
#elif    N_NOPS_T01L < 11
#  define  NOPS_T01L  NOPS_10
#elif    N_NOPS_T01L < 12
#  define  NOPS_T01L  NOPS_11
#elif    N_NOPS_T01L < 13
#  define  NOPS_T01L  NOPS_12
#elif    N_NOPS_T01L < 14
#  define  NOPS_T01L  NOPS_13
#elif    N_NOPS_T01L < 15
#  define  NOPS_T01L  NOPS_14
#elif    N_NOPS_T01L < 16
#  define  NOPS_T01L  NOPS_15
#elif    N_NOPS_T01L < 17
#  define  NOPS_T01L  NOPS_16
#elif    N_NOPS_T01L < 18
#  define  NOPS_T01L  NOPS_17
#elif    N_NOPS_T01L < 19
#  define  NOPS_T01L  NOPS_18
#elif    N_NOPS_T01L < 20
#  define  NOPS_T01L  NOPS_19
#elif    N_NOPS_T01L < 21
#  define  NOPS_T01L  NOPS_20
#elif    N_NOPS_T01L < 22
#  define  NOPS_T01L  NOPS_21
#else
#  error no valid NOPS_T01L
#endif


// Convert \n to _ to get shorter printout
#define  CVNL(str)  do{ for (char *cp=(str); *cp; ++cp)  if (*cp=='\n')  *cp='_'; } while (0)


int  main () {
    char nopst1L [] = NOPS_T1L;
    char nopst0H [] = NOPS_T0H;
    char nopst1H [] = NOPS_T1H;
    char nopst01L[] = NOPS_T01L;

    printf ("F_CPU: %u\n", F_CPU);

    CVNL(nopst1L);
    printf ("nopst1L : \"%s\"\n", nopst1L);

    CVNL(nopst0H);
    printf ("nopst0H : \"%s\"\n", nopst0H);

    CVNL(nopst1H);
    printf ("nopst1H : \"%s\"\n", nopst1H);

    CVNL(nopst01L);
    printf ("nopst01L: \"%s\"\n", nopst01L);

    printf ("N_INSTR_TOTAL: %d \t%f\n",
            N_INSTR_TOTAL, (double)(T0H_NOM+T0L_NOM)*F_CPU/1.e9 );

    printf ("NnopsT1L : %d \t%f\n", N_NOPS_T1L,
            (double)T1L_NOM*F_CPU/1.e9 - NC1L );

    printf ("NnopsT0H : %d \t%f\n", N_NOPS_T0H,
            (double)T0H_NOM*F_CPU/1.e9 - NC0H );

    // Put the float values between parentheses because not really relevant.
    // The calculation is actually done differently to be sure that the total
    // number of instruction cycles is kept correct.
    printf ("NnopsT1H : %d \t(%f)\n", N_NOPS_T1H,
            (double)T1H_NOM*F_CPU/1.e9 - NC1H );

    printf ("NnopsT01L: %d \t(%f)\n", N_NOPS_T01L,
               (double)(T0L_NOM - T1L_NOM + T0H_NOM)*F_CPU/1.e9 - NC0H
           );

    printf ("T1H: %f ns\n", (double)(N_NOPS_T1H+NC1H)/F_CPU*1.e9);
    printf ("T1L: %f ns\n", (double)(N_NOPS_T1L+NC1L)/F_CPU*1.e9);
    printf ("T1 : %f ns\n", (double)(N_NOPS_T1H+NC1H+N_NOPS_T1L+NC1L)/F_CPU*1.e9);
    printf ("T0H: %f ns\n", (double)(N_NOPS_T0H+NC0H)/F_CPU*1.e9);
    printf ("T0L: %f ns\n", (double)(N_NOPS_T01L+N_NOPS_T1L+NC0L)/F_CPU*1.e9);
    printf ("T0 : %f ns\n", (double)(N_NOPS_T0H+NC0H+N_NOPS_T01L+N_NOPS_T1L+NC0L)/F_CPU*1.e9);

    puts ("\nWith loop management:");
    //              CPU cycles
    // adiw           2
    // subi           1
    // cpse (no skip) 1
    // cpse (skip)    2
    // rjmp           2
    // In the loop (no skip)        : 2+1+1+2 = 6 CPU cycles
    // At the end of the loop (skip): 2+1+2+2 = 7 CPU cycles

    puts ("In the Loop:");
    printf ("T1L: %f ns\n", (double)(6+NC1L)/F_CPU*1.e9);
    printf ("T0L: %f ns\n", (double)(N_NOPS_T01L+6+NC0L)/F_CPU*1.e9);
    printf ("T1 : %f ns\n", (double)(N_NOPS_T1H+NC1H+6+NC1L)/F_CPU*1.e9);
    printf ("T0 : %f ns\n", (double)(N_NOPS_T0H+NC0H+N_NOPS_T01L+6+NC0L)/F_CPU*1.e9);
    puts ("At the end of the Loop:");
    printf ("T1L: %f ns\n", (double)(7+NC1L)/F_CPU*1.e9);
    printf ("T0L: %f ns\n", (double)(N_NOPS_T01L+7+NC0L)/F_CPU*1.e9);
    printf ("T1 : %f ns\n", (double)(N_NOPS_T1H+NC1H+7+NC1L)/F_CPU*1.e9);
    printf ("T0 : %f ns\n", (double)(N_NOPS_T0H+NC0H+N_NOPS_T01L+7+NC0L)/F_CPU*1.e9);
    return 0;
}

