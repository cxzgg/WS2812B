#!/usr/bin/perl -w

# Perl script to postprocess SVG files created by Fritzing Version 0.9.3
#  - Remove superfluous elements
#  - Change grey colors to black.
#
# Reads input from STDIN. writes output to STDOUT
#
# Version 1   24-Mar-2019
#
# Copyright (C) 2019  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#

use strict;

my @file = <>;
my $file = join '',@file;

$file =~ s!(font-size=("|'))0\2!${1}2.5${2}!sg;

# Remove elements like <text ...font-size="2.5" ...fill="#787878"...>...</text>
#$file =~ s!<text[^>]+?font-size=("|')2(\.5?(00[^'" >]*?)??)?\1[^>]+?fill=("|')#(787878|8c8c8c|8C8C8C|505050)\g{-2}.*?</text>!!sg;

# Remove elements like <text ...fill="#787878" ...font-size="2.5"...>...</text>
#$file =~ s!<text[^>]+?fill=("|')#(787878|8c8c8c|8C8C8C|505050)\1[^>]+?font-size=("|')2(\.5?(00[^'" >]*?)??)?\g{-3}.*?</text>!!sg;

$file =~ s!(<text[^>]+?fill=("|')#)(787878|8c8c8c|8C8C8C|505050)(\2)!${1}000000$4!sg;

$file =~ s!(<line[^>]+?stroke=("|')#)(787878|404040|555555)(\2[^>]*/>)!${1}000000$4!sg;
$file =~ s!(<path[^>]+?fill=("|')#)(787878|404040)(\2)!${1}000000$4!sg;
$file =~ s!(<path[^>]+?stroke=("|')#)(787878|404040)(\2)!${1}000000$4!sg;
$file =~ s!(<circle[^>]+?fill=("|')#)(787878)(\2)!${1}000000$4!sg;

$file =~ s!<g[^>]*/>!!sg;

while ($file =~ s!<g[^>]*>\s*</g>!!sg) {}
while ($file =~ s!\n\n!\n!sg) {}

print $file;
