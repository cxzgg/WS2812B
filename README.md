# WS2812B Driving Unit

Control unit to drive up to 300 LEDs connected via a WS2812B bus.

Version 2 2022-Sep-10th jost


## Features

 * The device can drive WS2812B LED chains of up to 300 LEDs.
 * The device creates colored dynamically changing patterns on the connected
   WS2812B LED chain.
 * The patterns are appropriate for a linear LED strip as well as WS2812B LEDs
   arranged in a matrix of 8×32 LEDs.
 * The patterns can be switched to a mode where a linear WS2812B LED strip is
   divided into two parts showing the same mirrored patterns. This offers
   the possibility to mount the LED chain to a bicycle such that it presents
   the same pattern on both sides of the bicycle.
 * The device is battery powered by three 1.5 V batteries of size D.

## Software

The project contains the software as **C++ source files** as well as the
resulting [**Intel hex file**](SW/build/ws2812b.hex) for firmware upload in
sub-directory [**SW/src**](SW/src) and [**SW/build**](SW/build) respectively.

A [**Makefile**](SW/Makefile) controlling the build and upload process can be
found in directory [**SW**](SW), type 'make help' in that directory for details.
It calls the **avr-gnu C++ compiler** to build the SW and
[**avrdude**](http://savannah.nongnu.org/projects/avrdude) to upload the SW to
the device.

Note that you have to adapt the parameters in the Makefile to the programmer and
programmer port you use.

Perl scripts to create fuse and lock bytes and to interprete existing lock and
fuse bytes are provided in sub-directory [**SW/bytes**](SW/bytes).


## Hardware Description

The project uses an AVR **ATmega328P** microcontroller.

*Schematic* and *PCB specifications* for two different enclosure variants are
available as **Fritzing 0.9.3** projects in directory [**Fritzing**](Fritzing)
and as according exports in sub-directory
[**Fritzing/exports**](Fritzing/exports).

Sub-directory [**Fritzing/parts**](Fritzing/parts) contains the parts used in
the Fritzing project and not available in the standard Fritzing part library.

Directory [**enclosure**](enclosure) contains the drawing for one of the two
enclosure variants.


## Analysis, Requirements, Design

The file [**AnaReqDes.txt**](AnaReqDes.txt) contains an analysis and the
requirements of the project, as well as the results of the design decisions made
in the project.

File [**AnaReqDes.html**](AnaReqDes.html) contains the same converted to HTML
using the script [**mkhtml.pl**](https://codeberg.org/cxzgg/mkhtml.pl.git).

Directory [**design**](design) contains some design models, see comments in
those files and [AnaReqDes.html](AnaReqDes.html) for details.


## Documentation

A detailed user manual is provided in sub-directory [**doc**](doc) as
**docbook 5.0** source ([doc/WS2812B.xml](doc/WS2812B.xml)) and as compiled
**PDF**, **HTML**, and **text** results in English and German language.

The images of PCB and the schematic used in the documentation are manually
modified versions of the Fritzing exports.

A [Makefile](doc/Makefile) to control the compilation of the documention can be
found in the same directory, type 'make help' in that directory for details.

The project uses _xsltproc (libxslt)_
[http://www.xmlsoft.org/](http://www.xmlsoft.org/)
and Perl to create the HTML documention and intermediate XML files. It uses
_w3m_
[https://sourceforge.net/projects/w3m/](https://sourceforge.net/projects/w3m/)
to create the text version of the documention and _xmlgraphics fop_
[https://xmlgraphics.apache.org/fop/](https://xmlgraphics.apache.org/fop/)
to create the PDF version.


## License

Software:
  GNU General Public License Version 3, see file
  [SW/src/COPYING](SW/src/COPYING) or <https://www.gnu.org/licenses/gpl-3.0>.

Documentation and Fritzing project:
  Creative Commons Attribution-NonCommercial-ShareAlike 4.0 Unported License:
  <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
