#!/bin/sh

# Combine the PPM files to multi-image TIFs
#
# This script need ImageMagick being installed

convert cyclist*.ppm -adjoin cyclist.tif
convert star*.ppm -adjoin star.tif
convert lines*.ppm -adjoin lines.tif
convert ball*.ppm -adjoin ball.tif
convert bubbles*.ppm -adjoin bubbles.tif
