//##############################################################################
// Interface Definitions for Serial Interface
//
// File serial.h
//
// Copyright (C) 2017  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the license, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//

#ifndef serial_H
#define serial_H

char     *itoa (uint32_t, uint8_t, char[]);
void      serialSetup (void);
uint8_t   serialGetChar (void);
void      serialTransmit (const char*);
void      serialTransmitValue (int32_t);

#endif  // serial_H
