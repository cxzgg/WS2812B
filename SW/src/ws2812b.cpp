//##############################################################################
//
// WS2812B Driving Unit
//
// Control multiple LEDs each driven by a WS2812B chip
//
// Version 2    2022 Sep 10th   jost.brachert@gmx.de
//              Adaptation to make different CPU clock rates possible.
//
// Version 1    2017 Jan 14th   jost.brachert@gmx.de
//
// The program is intended to be run on an ATMEL ATmega328P with 12 to 20 MHz
// system clock frequency
//
// Copyright (C) 2017-2022  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the license, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//

#include <stdint.h>          // (u)int8_t,(u)int16_t,(u)in32_t
#include <string.h>          // memset(),memcpy()
#include <avr/io.h>          // DDRx,PORTx,PINx,TCCRxx,TIMSKx,...
#include <avr/interrupt.h>   // sei(),cli()
#include <avr/wdt.h>         // wdt_reset()
#include <avr/pgmspace.h>    // PROGMEM,memcpy_P(),pgm_read_byte()

#include "serial.h"          // serialSetup(),serialTransmit(),
                             // serialTransmitValue()

#define  VERSION  "2"


#ifndef __AVR_ATmega328P__   // Verify correct library for part, see avr/io.h
#error Program is for ATmega328P only
#endif


// Decide whether to measure and transmit the maximum time of the main loop
// cycle via serial interface
#define RUNTIME_MEASUREMENT


#if defined(__GNUC__)
#   define forceinline __attribute__((always_inline)) inline
#else
#   warning Forcing inline is required to fulfill the timing requirements
#   define forceinline inline
#endif


//------------------------------------------------------------------------------
// CPU Timing Parameters
//
// Note: Those values must not be changed without checking the timer
// initialization code, see function setTimers():

#ifndef F_CPU
#error F_CPU not defined
#endif

#if F_CPU < 12000000
#error F_CPU too low. F_CPU must be >= 12 MHz
#endif

#if F_CPU > 20000000
#error F_CPU too high. F_CPU must be <= 20 MHz
#endif

                                    // Period of system time counter increments
#define  TIMEBASE    0.004          // Timer 2 overflow period [s]
                                    // Max. valid value 0xFF*clkDiv/F_CPU=0.013

// Maximum integer value for all time parameters is
//   INT8_MAX - 1 - N = 127 - N
// and thus
//   (127-N)*TIMEBASE [s]
// for the physical time values in seconds, where N is the maximum increment
// of timeCpy between successive calls to loop().
// N is less or equal 10 - however, check with RUNTIME_MEASUREMENT if code is
// changed.

// Wait for specified number of microseconds (us)
// Relation between counter value N and us is us = N*8e6/F_CPU + 2.56e6/F_CPU
// Maximum value for us is 255*8e6/F_CPU + 2.56e6/F_CPU
//   For F_CPU =  1000000 Hz usmax is 2042.56 us
//   For F_CPU =  2000000 Hz usmax is 1021.28 us
//   For F_CPU =  8000000 Hz usmax is  255.3 us
//   For F_CPU = 16000000 Hz usmax is  127.6 us
//   For F_CPU = 20000000 Hz usmax is  102.1 us
// Minimum value for us is 1
// Resolution in us is 1*8e6/F_CPU + 2.56e6/F_CPU
//   For F_CPU =  1000000 Hz resolution is 10.56 us
//   For F_CPU =  2000000 Hz resolution is  5.28 us
//   For F_CPU =  8000000 Hz resolution is  1.32 us
//   For F_CPU = 16000000 Hz resolution is  0.66 us
//   For F_CPU = 20000000 Hz resolution is  0.53 us
// This is at the same time also the minimal allowed value
#define  WAITCONST      (2.56e6/(F_CPU))
#define  WAITFACT       (8.00e6/(F_CPU))
#define  WAIT_US(us)    do {\
                          volatile uint8_t w=((us)-(WAITCONST))/(WAITFACT)+0.5;\
                          while(--w!=0){}\
                        } while(0)


//------------------------------------------------------------------------------
// Pin Configuration

// Port D, PD1, Pin 3, Ard.Uno Pin 1 is reserved for serial interface TX
static const int  ws2812Bit  = 0;   // Port B, PB0, Pin 14, Ard.Uno Pin  8

// Note: switch0 of the 4-pin DIP switch is not connected
static const int  switch1Bit = 2;   // Port D, PD2, Pin  4, Ard.Uno Pin  2
static const int  switch2Bit = 3;   // Port D, PD3, Pin  5, Ard.Uno Pin  3
static const int  switch3Bit = 4;   // Port D, PD4, Pin  6, Ard.Uno Pin  4

//------------------------------------------------------------------------------
// Permanently bind registers r3 and r4 to the WS2812B output bit masks for
// setting and resetting the pin to make them accessible by the out instruction
// in the assembler output routine.

register uint8_t  maskHi asm("r3");
register uint8_t  maskLo asm("r4");

//------------------------------------------------------------------------------
// Basic Parameters

#define  NLEDS    300         // Total number of LEDs
#define  NCIRCLE  250         // See below at "Definition of LED Color Patterns"

#define  DISPLAYWIDTH     32  // Total width of the 2-dim. display screen
#define  DISPLAYHEIGHT     8  // Total height of the 2-dim. display screen
#define  NDISPLAYLEDS   ((DISPLAYWIDTH)*(DISPLAYHEIGHT))

#define  COLORCHANNELS  3     // Red, green, blue

#define  GREEN          0     // Order of the color channels on the WS2812B bus
#define  RED            1
#define  BLUE           2

#define  HUE            0
#define  BRIGHTNESS     1
#define  SATURATION     2


//------------------------------------------------------------------------------
// Definition of LED Color Patterns

// A color pattern table defines the color characteristic of all NLEDS LEDs.
//
// The algorithm assumes that the LEDs are mounted in a circle of NCIRCLE LEDs.
// Dynamic patterns are organized such that a connection of the first LED to the
// NCIRCLEth LED provokes an impression of a seamlessly circulating pattern
// The patterns wrap therefore around at NCIRCLE.
// The LEDs from NCIRCLE to NLEDS are treated as a tail branching off at
// LED no. NCIRCLE. The pattern at those LEDs is duplicated for the LEDs
// 0...NLEDS-NCIRCLE.
//
// The total number NCIRCLE of the LEDs in the LED circle is divided into a
// number of segments.
// Each segment has an entry in the pattern table. That entry defines the
// color of the first LED in that segment. The color of the last LED in that
// segment is the color definition of the next table entry or the first one if
// the considered entry is the last non-null entry in the list.
// The color of all LEDs between those endpoints of the segment is determined by
// linear interpolation between those endpoints.
//
// The sum of the sizes of all segments is thus supposed to be equal to NCIRCLE.
//

struct PatternEntry {
    uint8_t   color[COLORCHANNELS];     // Green, red, and blue color channel
                                        //   at the start of the segment
                                        // cppcheck-suppress cert-API01-C
    uint16_t  n;                        // Number of LEDs in the segment
};


// Last entry of each table must be a null entry. This is used to determine
// the end of the table.

// Continuous spectrum
static const struct PatternEntry  patternTable0_init[] PROGMEM =
{
    //  gn  red   bl   n
    { {  0, 255,   0}, NCIRCLE/8 },             // 0
    { {255, 255,   0}, NCIRCLE/8 },             // 1
    { {255,   0,   0}, NCIRCLE/8 },             // 2
    { {255,   0, 255}, NCIRCLE/8 },             // 3
    { {  0,   0, 255}, NCIRCLE/8 },             // 4
    { {  0, 255, 255}, NCIRCLE/8 },             // 5
    { { 64, 128, 255}, NCIRCLE - 6*NCIRCLE/8},  // 6
    { {  0,   0,   0}, 0 },                     // 7
};
static struct PatternEntry  patternTable0 [
                                  sizeof( patternTable0_init)
                                / sizeof(*patternTable0_init)
                            ];

// One pink spot
static const struct PatternEntry  patternTable1_init[] PROGMEM =
{
    //  gn  red   bl    n
    { {  0,   0,   0},  1 },                    // 0
    { {  0, 255, 255}, 10 },                    // 1
    { {  0, 255, 255},  1 },                    // 2
    { {  0,   0,   0}, NCIRCLE -1 -10 -1},      // 3
    { {  0,   0,   0},  0 },                    // 4
};
static struct PatternEntry  patternTable1 [
                                  sizeof( patternTable1_init)
                                / sizeof(*patternTable1_init)
                            ];

// One cyan spot
static const struct PatternEntry  patternTable2_init[] PROGMEM =
{
    //  gn  red   bl    n
    { {  0,   0,   0},  1 },                     // 0
    { {255,   0, 255}, 10 },                     // 1
    { {255,   0, 255},  1 },                     // 2
    { {  0,   0,   0}, NCIRCLE -1 -10 -1},       // 3
    { {  0,   0,   0},  0 },                     // 4
};
static struct PatternEntry  patternTable2 [
                                  sizeof( patternTable2_init)
                                / sizeof(*patternTable2_init)
                            ];

#if 1
// Rainbow spot
static const struct PatternEntry  patternTable3_init[] PROGMEM =
{
#define  STRIPEWIDTH    5
    //  gn  red   bl             n
    { {  0,   0,   0},           1 },                   // 0
    { {  0, 255,   0}, STRIPEWIDTH },                   // 1
    { {255, 255,   0}, STRIPEWIDTH },                   // 2
    { {255,   0,   0}, STRIPEWIDTH },                   // 3
    { {255,   0, 255}, STRIPEWIDTH },                   // 4
    { {  0,   0, 255}, STRIPEWIDTH },                   // 5
    { {  0, 255, 255}, STRIPEWIDTH },                   // 6
    { {  0,   0,   0}, NCIRCLE - 6*STRIPEWIDTH -1},     // 7
    { {  0,   0,   0},           0 },                   // 8
};
static struct PatternEntry  patternTable3 [
                                  sizeof( patternTable3_init)
                                / sizeof(*patternTable3_init)
                            ];
#endif

// White ramp down, ramp up
static const struct PatternEntry  patternTable4_init[] PROGMEM =
{
    //  gn  red   bl   n
    { {255, 255, 255}, NCIRCLE/2},                  // 0
    { { 10,  10,  10}, NCIRCLE - NCIRCLE/2},        // 1
    { {  0,   0,   0}, 0 },                         // 2
};
static struct PatternEntry  patternTable4 [
                                  sizeof( patternTable4_init)
                                / sizeof(*patternTable4_init)
                            ];


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// List of pointers to pattern tables for initialization
// NOTE: Every pattern table defined above must get an entry here

static const struct PatternInitListEntry {
          struct PatternEntry  *patternTable;
    const struct PatternEntry  *patternTable_init;
    const uint8_t               sizeOfTable;
}  patternInitList[] = {
    { patternTable0, patternTable0_init, sizeof(patternTable0_init) },
    { patternTable1, patternTable1_init, sizeof(patternTable1_init) },
    { patternTable2, patternTable2_init, sizeof(patternTable2_init) },
    { patternTable3, patternTable3_init, sizeof(patternTable3_init) },
    { patternTable4, patternTable4_init, sizeof(patternTable4_init) },
};


//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// struct Pattern
//
// noOfCycles:   A cycle is the time for one updatePeriod.
//               After a number of noOfCycles cycles (i.e. updatePeriods) the
//               algorithm switches to the next pattern or image.
// weight1:      Weight of pattern in table1 (0...256,256=100%), even if there
//               is no superposition
// updatePeriod: Time between updates of the currently displayed pattern
// table1:       Pointer to a color pattern as described above
// table2:       Two patterns are superposed if table2!=NULL.
//               The table2 pattern is always weighted with 100%. The patterns
//               are added and the result limited to 255.

struct Pattern {
    const uint16_t                    noOfCycles;
    const uint16_t                    weight1;
    const int8_t                      updatePeriod;
    const struct PatternEntry *const  table1;
    const struct PatternEntry *const  table2;
};

static const struct Pattern  patternList[] = {
    {200, 256, (int8_t)(0.100/TIMEBASE+0.01), patternTable0, NULL},
    {200, 256, (int8_t)(0.040/TIMEBASE+0.01), patternTable1, NULL},
    {200, 256, (int8_t)(0.040/TIMEBASE+0.01), patternTable2, NULL},
    {200, 256, (int8_t)(0.040/TIMEBASE+0.01), patternTable3, NULL},
    {200, 256, (int8_t)(0.040/TIMEBASE+0.01), patternTable4, NULL},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable0, patternTable1},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable1, patternTable1},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable2, patternTable1},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable3, patternTable1},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable4, patternTable1},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable0, patternTable2},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable1, patternTable2},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable2, patternTable2},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable3, patternTable2},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable4, patternTable2},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable0, patternTable3},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable1, patternTable3},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable2, patternTable3},
    {200,  64, (int8_t)(0.100/TIMEBASE+0.01), patternTable3, patternTable3},
    {200,  64, (int8_t)(0.050/TIMEBASE+0.01), patternTable4, patternTable3},
};


static int16_t  nLeds        = NLEDS;
static int16_t  nCircle      = NCIRCLE;
static int16_t  nDisplayLeds = NDISPLAYLEDS;
static uint8_t  displayWidth = DISPLAYWIDTH;


//------------------------------------------------------------------------------
// Definition of Complete 2-dim. Images
//
// The values are computed by program mkimages
// See design issues [FuDsPrecompImageData] [FuDsSepCalcImageData]

#include  "mkimages/images.h"


//------------------------------------------------------------------------------
// Definition of Modulation Type

struct Modulation {
    int16_t   max;        // Modulation parameter: maximum value
    int16_t   min;        // Modulation parameter: minimum value
    int16_t   down;       // Modulation step size upwards
    int16_t   up;         // Modulation step size downwards
    int16_t   delta;      // Current step size, either equal up or down
    int16_t   mod;        // Modulation state, current modulation value
};


//------------------------------------------------------------------------------

static uint16_t  modValue (Modulation*);
static void      setLed (int16_t, const uint8_t*);
static void      setPatternLeds (const struct PatternEntry*, int16_t, uint16_t,
                                 uint16_t, uint16_t, uint16_t);
static void      setImageLeds (const struct Image*, int16_t, uint16_t, uint16_t,
                               uint16_t);
static uint8_t   lcg16_8 (void);
static void      memcpyRev (uint8_t*, uint8_t*, int16_t);
static void      setTimers (void);
static void      ws2812out (void);
static forceinline void  ws2812out_OneLoop (uint8_t);


static          uint8_t  msg[3*NLEDS];  // RGB LED value array to be transmitted
static volatile uint8_t  timeCnt;                  // Global system time counter
static const    char     cNewLine[] = "\x0d\x0a";





//==============================================================================
// Main Program

int  main (void)
{
    static uint8_t   patIdx;

    //--------------------------------------------------------------------------
    // Disable interrupts globally during initialization

    cli();

    //--------------------------------------------------------------------------
    // Watchdog
    //
    // ATmega48A/PA/88A/PA/168A/PA/328/P [DATASHEET]
    // Atmel-8271J-AVR-ATmega-Datasheet_11/2015
    // 11.8.2 Watchdog Timer Overview:
    // To avoid an eternal loop of time-out resets the application software
    // should always clear the Watchdog System Reset Flag (WDRF) and the
    // WDE control bit in the initialization routine, even if the Watchdog is
    // not in use.

    // Trigger the watchdog to clear an accidentally pending interrupt
    wdt_reset();

    // Clear WD System Reset Flag to recover savely from a (possibly
    // unintentional) Watchdog System Reset and avoid an eternal reset loop
    // by accidentally enabled watchdog
    MCUSR &= ~(1<<WDRF);

    // Initiate changing the WDT parameters by setting Watchdog Change
    // Enable (WDCE) and Watchdog System Reset Enable (WDE)
    WDTCSR = (1<<WDCE) | (1<<WDE);
    // 4 cycles from here to set the new WDT parameters ...

    // Stop Watchdog: disable WD System Reset (WDE) and WD interrupt (WDIE)
    WDTCSR = 0;


    //--------------------------------------------------------------------------
    // Define the bitmasks in the WS2812B output port for the WS2812B output pin
    // Note that these bitmasks are permanently tied to registers r3 and r4.

    maskHi =   1<<ws2812Bit;
    maskLo = ~(1<<ws2812Bit);

    //--------------------------------------------------------------------------
    // Initialize ADC

    // Disable digital input on ADC channel 0
    DIDR0 = (1<<ADC0D);

    // ADC channel number of potentiometer input
#define  ADC_POTI    0

    // Macro for ADC reference voltage and muxer settings
    // - Select internal 1.1V voltage reference
    // - Adjust ADC value left
    // - Set ADC channel

#define  SETADMUX(ch)   ((1<<REFS1) | (1<<REFS0) | (1<<ADLAR) | (ch))
    ADMUX = SETADMUX(ADC_POTI);     // Set ADC reference and initialize muxer

    ADCSRB = 0;                     // Disable Analog Comparator Multiplexer
                                    // Set Free Running mode
#define  ADCPRESCLDIV16     7       // Define ADC Prescaler Index for Div:128
    ADCSRA =   (1<<ADEN)            // Turn on ADC
             | (0<<ADIE)            // Don't enable ADC interrupt
             | ADCPRESCLDIV16       // Set ADC Prescaler
             | (1<<ADSC);           // Start ADC conversion

    // Settling of the voltage reference needs max 70us, conversion should be
    // started earliest 70us after setting ADEN, see section 24.4 "Prescaling
    // and Conversion Timing" in the ATmega328P Datasheet.
    // However, a short deviation of the measured potentiometer value does not
    // matter. Therefore no further action to avoid that.

    //--------------------------------------------------------------------------
    // Set up Timers and Digital I/O

    setTimers ();
                                    // Set direction "Output" for the port pins
    DDRB = (1<<ws2812Bit);
                                    // Activate pull-ups for Port D input pins
    PORTD = (1<<switch1Bit) | (1<<switch2Bit) | (1<<switch3Bit);


    //--------------------------------------------------------------------------
    // Reenable interrupts globally

    sei();


    //--------------------------------------------------------------------------
    // Send startup message

    serialSetup ();

    serialTransmit ("WS2812B Driving Unit\x0d\x0aVersion " VERSION "\x0d\x0a");


    //--------------------------------------------------------------------------
    // Initialize patterns

    for (uint8_t i=0; i<sizeof(patternInitList); ++i) {
        const PatternInitListEntry *const  p = &patternInitList[i];
        memcpy_P (p->patternTable,
                  p->patternTable_init,
                  p->sizeOfTable);
    }

    //--------------------------------------------------------------------------
    // Reinitialize the global time counter because the setup routine might
    // otherwise have consumed too much of the first time slice so that not
    // enough time is left for the first main loop cycles.

    timeCnt = 0;


    //==========================================================================
    // Main Loop

    for (;;) {
        static uint8_t   mirror;        // =1: mirror two halves
#define  INITIAL_PERIOD      0.1        // Initial LED update period [s]
        static int8_t    updatePeriod = INITIAL_PERIOD/TIMEBASE + 0.01;
        static uint16_t  noOfCycles = 200;
        static uint8_t   timeSave;
        static uint8_t   poti;
        static int16_t   offset1;       // Shift a pattern along the LEDs or
                                        // horizontally over the display
        static int16_t   offset2;       // Shift the 2nd pattern along the LEDs
        static uint8_t   useImages;     // =0: patterns, =1: 2-dim. images
//        static uint8_t   patIdx;
        static uint16_t  cycleCnt;
        static uint8_t   blink;         // =0: No blink effect
                                        // =1, =2, =4: Blink mask
        static uint8_t   imgIdx;
                                        // max min dwn up d mod
#define  NMODC    3
        static struct Modulation  modC[NMODC] = {
            //    max min dwn up del mod
                { 128,  0, -1, 2, 2, 0 },
                { 128, 10, -2, 1, 1, 0 },
                { 128, 13, -3, 2, 2, 0 },
            };
        static struct Modulation  modW = { 256, 0, -1, 1, 1, 0 };
        static const  uint8_t  modIdxUpdCycles = 10;
        static        uint8_t  modIdxUpdCnt;
        static        uint8_t  modIdx;

        uint16_t  widthMod;     // Factor to modify LED pattern segment width
        uint16_t  greenMod;     // Factor to modify the green color channel
        uint16_t  redMod;       // Factor to modify the red color channel
        uint16_t  blueMod;      // Factor to modify the blue color channel

        uint8_t  timeCpy;       // Local copy of global system time


        timeCpy = timeCnt;

        if ((int8_t)(timeCpy - timeSave) >= updatePeriod) {

            ws2812out ();

            //------------------------------------------------------------------
            // Read Switches

            if (PIND & (1<<switch1Bit)) {
                if (mirror) {
                    nLeds        = NLEDS;
                    nCircle      = NCIRCLE;
                    nDisplayLeds = NDISPLAYLEDS;
                    displayWidth = DISPLAYWIDTH;

                    // Restore the original pattern segment sizes
                    for ( uint8_t j=0;
                          j< sizeof(patternInitList) / sizeof(*patternInitList);
                          ++j
                        ) {
                        const PatternInitListEntry *const  p = &patternInitList[j];
                        for ( uint8_t i = 0;
                              i < p->sizeOfTable / sizeof(struct PatternEntry);
                              ++i) {
                            p->patternTable[i].n = pgm_read_byte (
                                                    &(p->patternTable_init[i].n)
                                                   );
                        }
                    }
                }
                mirror = 0;
            } else {
                if ( ! mirror) {
                    nLeds        = NLEDS/2u;
                    nCircle      = NCIRCLE/2u;
                    nDisplayLeds = NDISPLAYLEDS/2u;
                    displayWidth = DISPLAYWIDTH/2u;
                    offset1 = 0;
                    offset2 = 0;

                    // Halve the pattern segment sizes
                    for ( uint8_t j=0;
                          j< sizeof(patternInitList) / sizeof(*patternInitList);
                          ++j
                        ) {
                        const PatternInitListEntry *const  p = &patternInitList[j];
                        for ( uint8_t i = 0;
                              i < p->sizeOfTable / sizeof(struct PatternEntry);
                              ++i) {
                            p->patternTable[i].n = pgm_read_byte (
                                                    &(p->patternTable_init[i].n)
                                                   );
                            if ( ! p->patternTable[i].n)  p->patternTable[i].n = 1;
                        }
                    }
                }
                mirror = 1;
            }

            //------------------------------------------------------------------
            // Determine modulation factors
            // See design issue FuDsFactNorm regarding normalization of integer
            // arithmetik.

            redMod   = modValue (&modC[(modIdx+0)%NMODC]) * poti/256u;
            greenMod = modValue (&modC[(modIdx+1)%NMODC]) * poti/256u;
            blueMod  = modValue (&modC[(modIdx+2)%NMODC]) * poti/256u;
            widthMod = modValue (&modW);

            //------------------------------------------------------------------
            // Determine and put out LED color patterns and 2-dim. images
            if ( ! useImages) {
                // Display color patterns

                if (blink && (cycleCnt & 0x2)) {
                    redMod   = 0;
                    greenMod = 0;
                    blueMod  = 0;
                }

                memset (msg, 0, 3*nLeds);

                // Two patterns are superposed if patternList[patIdx].table2!=0
                setPatternLeds (patternList[patIdx].table1, offset1,
                        redMod  *patternList[patIdx].weight1/256u,
                        greenMod*patternList[patIdx].weight1/256u,
                        blueMod *patternList[patIdx].weight1/256u, widthMod);
                if (patternList[patIdx].table2) {
                    setPatternLeds (patternList[patIdx].table2, -offset2,
                        redMod,
                        greenMod,
                        blueMod, widthMod);
                }

                if ((offset1+=1) >= nCircle)  offset1 = 0;
                if ((offset2+=1) >= nCircle)  offset2 = 0;
            } else {
                // Display two-dimensional images
                memset (msg, 0, 3*nLeds);    // Three color channels
                setImageLeds (&images[imgIdx], offset1, redMod, greenMod, blueMod);

                // Extend the image to LEDs nDisplayLeds...nLeds
                memcpy (msg+3*nDisplayLeds, msg, 3*(nLeds-nDisplayLeds));

                if (++offset1 >= images[imgIdx].nImages * displayWidth) {
                    offset1 = 0;
                }
            }
            if (mirror) {
                // NOTE that the function copies the bytes in reverse order.
                // That means that the color channels green and blue are
                // exchanged in the copy. However, that doesn't matter for the
                // visual impression, but the copying is faster.
                memcpyRev (msg+3*NLEDS/2, msg, 3*NLEDS/2);
            }


            //------------------------------------------------------------------
            // Check whether the time for the current pattern or image is over
            if (++cycleCnt >= noOfCycles) {
                // Move to next pattern or image and set the pattern specific
                // values updatePeriod and noOfCycles accordingly
                useImages = lcg16_8() >= (uint8_t)(UINT8_MAX*0.7 +0.5);
                if ( ! useImages) {
                    if (++patIdx >= sizeof(patternList)/sizeof(*patternList)) {
                        patIdx = 0;
                    }
                    updatePeriod = patternList[patIdx].updatePeriod;
                    noOfCycles   = patternList[patIdx].noOfCycles;

                    if (lcg16_8() < (uint8_t)(UINT8_MAX*0.125 +0.5)) {
                        uint8_t  r = lcg16_8();
                        if (r > 127)  blink = 0x2;
                        else          blink = 0x1;
                    } else {
                        blink = 0;
                    }
                } else {
                    if (++imgIdx >= NIMAGES)  imgIdx = 0;
                    updatePeriod = images[imgIdx].updatePeriod;
                    noOfCycles   = images[imgIdx].noOfCycles;
                }

                if (++modIdxUpdCnt >= modIdxUpdCycles) {
                    if (++modIdx >= NMODC) {
                        modIdx = 0;
                    }
                    modIdxUpdCnt = 0;
                }

                cycleCnt = 0;
                offset1 = 0;
                offset2 = 0;
            }


            //------------------------------------------------------------------
            // Read ADC of potentiometer input

            if ( ! (ADCSRA & (1<<ADSC))) {   // If current conversion finished
                poti = ADCH;                 // Read the ADC value
                ADCSRA |= 1<<ADSC;           // Start next ADC conversion
            }

            timeSave += updatePeriod;

#ifdef RUNTIME_MEASUREMENT
            uint8_t  timeCpy1 = timeCnt;
            uint8_t  timeDiff = timeCpy1 - timeCpy;

            serialTransmitValue (patIdx);
            serialTransmit ("  ");

            serialTransmitValue (timeDiff);
            serialTransmit (cNewLine);
#endif
        }
    }

    return 0;
}



//==============================================================================
// setImageLeds
//
// Put a monochrome image sequence with the color value (red,green,blue) to
// array msg. msg will later be sent to the WS2812B bus.
//
// Argument offset determines the index of the subimage to display currently
// and its horizontal shift on the display.
// offset must not be negative.
//
// offset % image->nImages   is the index of the subimage.
//
// (offset / image->nImages) * image->shift   is the horizontal shift of it.
//
// Global variables:
// msg
// nDisplayLeds
// displayWidth

static void  setImageLeds (
    const struct Image *const  image,  // Ptr to image sequence
          int16_t   offset,     // Shift the image horizontally over the display
                                //   and select subimage from the image sequence
    const uint16_t  red,        // Color value of the image to send (0...255)
    const uint16_t  green,      //    "
    const uint16_t  blue        //    "
) {
    int16_t  i;

    // Note: the displayed object in the images has an horizontal offset
    //       in the image that can vary between the images of a sequence
    //       If the image sequence is played again it has to be shifted
    //       horizontally by exactly that offset (modulo the LED display
    //       width).
    // offsetX
    //       is the offset between complete image sequences
    //       in number of columns. If images follow directly each other then
    //       offsetX is the horizontal size ("width") of the image in number
    //       of columns.
    // subOffset
    //       is the index of the current image in the image sequence
    // ot    is the offset of the current image in number of LEDs
    // offs  is the offset of the current image in number of columns
    // i     is the total index of the first pixel of a column of the currwnt
    //       image
    // j     is the height index in a column
    // iLed  is the total index of the first LED of a column
    // iL    is the total index of a particular LED

    uint8_t  subOffset = offset % image->nImages;
    uint8_t  offsetX   = offset / image->nImages;

    int16_t  offs = offsetX*image->shift;

    while (offs >= displayWidth)  offs -= displayWidth;

    for (i=0; i<image->spritePixels; i+=DISPLAYHEIGHT) {
        int16_t  iLed;

        iLed = i + offs*DISPLAYHEIGHT;
                                              // Actually: iLed1 %= nDisplayLeds
        while (iLed >= nDisplayLeds)  iLed -= nDisplayLeds;

        for (uint8_t j=0; j<DISPLAYHEIGHT; ++j) {
            int16_t  iL;
            // At every other column changes the direction
            if ( ! (offs & 1))  iL = iLed + j;
            else                iL = iLed + (DISPLAYHEIGHT-1) - j;
            // NOTE that the order of colors channels changes between the
            // storage in memory (RGB) and in the message (GRB)

            msg[iL*3 + 0] = pgm_read_byte (image->data[subOffset*image->spritePixels + (i+j)]+1) * green/256u;
            msg[iL*3 + 1] = pgm_read_byte (image->data[subOffset*image->spritePixels + (i+j)]+0) * red  /256u;
            msg[iL*3 + 2] = pgm_read_byte (image->data[subOffset*image->spritePixels + (i+j)]+2) * blue /256u;
        }
    }
}



//==============================================================================
// setPatternLeds
//
// Put a pattern defined by patternTable to the message array msg[] to be later
// sent on the WS2812B bus.
// For details regarding the pattern table see the description at the definition
// of the PatternEntry structure above.
//
// The pattern is put to the array by addition so that several calls to
// setPatternLeds() will superpose the patterns.
//
// offset        : Shifts the pattern along the LED chain, may be negative
// red,green,blue: Modulation color value to be multiplied to the pattern's own
//                 color characteristic
// width         : The segment size can be reduced to width/256u.
//                 In that case loop counter i doesn't reach the loop end
//                 nCircle after all segments are processed once.
//                 For that case has the outer loop for(;;){..} been added.
//                 That loop starts over until i reaches nCircle and iLED
//                 reaches nLeds.
//                 The pattern is thus compressed and manifold replicated.
//
// Global variables:
// nLeds
// nCircle
//

static void  setPatternLeds (
    const struct PatternEntry  *const patternTable,
          int16_t   offset,
    const uint16_t  red,
    const uint16_t  green,
    const uint16_t  blue,
    const uint16_t  width
) {
    int16_t  i;
    int16_t  iLed = 0;

    // Actually: offset %= nCircle:
    while (offset >= nCircle)  offset -= nCircle;
    while (offset <     0   )  offset += nCircle;

    i = 0;
    for (;;) {
        const struct PatternEntry  *patternP1;   // Ptr to segment def. w/ 1st color
        const struct PatternEntry  *patternP2;   // Ptr to 2nd color

        // Loop through all entries in the pattern table until the null-entry
        for (patternP1=patternTable; patternP1->n; ++patternP1) {
            patternP2 = patternP1 + 1;
            if (patternP2->n == 0)  patternP2 = patternTable;

            // Number of LEDs in the segment to be interpolated
            int16_t  dj = (uint32_t)patternP1->n * width / 256u;
            if (dj == 0)  dj = 1;

            const uint8_t  *color1 = patternP1->color;     // 1st color, start of interpolation
            const uint8_t  *color2 = patternP2->color;     // 2nd color, end of interpolation

            for (int16_t j=0; j<dj; ++j) {
                uint8_t  color[COLORCHANNELS];

                // Linear interpolation between the colors c1 and c2
                color[0] = color1[0] + (int16_t)(color2[0]-color1[0])*j/dj;
                color[1] = color1[1] + (int16_t)(color2[1]-color1[1])*j/dj;
                color[2] = color1[2] + (int16_t)(color2[2]-color1[2])*j/dj;

                color[0] = (uint16_t)color[0] * green / 256u;
                color[1] = (uint16_t)color[1] * red   / 256u;
                color[2] = (uint16_t)color[2] * blue  / 256u;

                iLed = i + offset;

                if (i < nCircle) {
                    int16_t  iLed1 = iLed;
                    // Actually: iLed1 %= nCircle
                    while (iLed1 >= nCircle)  iLed1 -= nCircle;
                    setLed (iLed1, color);
                }

                if (iLed >= nCircle && iLed < nLeds) {
                    setLed (iLed, color);
                }

                ++i;

                if (iLed >= nLeds && i >= nCircle)  return;
            }
            if (iLed >= nLeds && i >= nCircle)  return;
        }
        if (iLed >= nLeds && i >= nCircle)  return;
    }
}



//==============================================================================
// setLed
//
// Put color "color" to LED with index "iLed" in the RGB LED array msg[].
//
// The color is put to array by addition so that multiple calls to setLed() for
// the same iLed add up to a final color value.
// On overflow the color channel values are limited to 255.
//
// Global variables:
// msg


static void  setLed (
          int16_t         iLed,
    const uint8_t *const  color
) {
    uint16_t  c;

    iLed *= 3;

    c = (uint16_t)msg[iLed + 0] + color[0];
    if (c > 255)  c = 255;
    msg[iLed + 0] = c;

    c = (uint16_t)msg[iLed + 1] + color[1];
    if (c > 255)  c = 255;
    msg[iLed + 1] = c;

    c = (uint16_t)msg[iLed + 2] + color[2];
    if (c > 255)  c = 255;
    msg[iLed + 2] = c;
}



//==============================================================================
// modValue
//
// This function modifies the modulation state modP->mod linearly between
// the parameters modP->max and modP->min.
//
// The speed of the change is modP->delta per function call, that is
// modP->delta per updatePeriod.
//
// modP->mod is limited to positive values.
//

static uint16_t  modValue (
    struct Modulation  *modP                // Ptr to modulation state
) {
    modP->mod += modP->delta;
    if (modP->mod > modP->max) {            // If beyond max. then
        modP->delta = modP->down;           //   turn direction
        modP->mod  += modP->delta;          //   step back below max.
    } else if (modP->mod < modP->min) {     // If beyond min. then
        modP->delta = modP->up;             //   turn direction
        modP->mod  += modP->delta;          //   step back above min.
    }
    if (modP->mod < 0)  modP->mod = 0;
    return  modP->mod;
}



//==============================================================================
// lcg16_8
//
// Linear Congruential Pseudo Random Number Generator with 16 bit state and
// 8 bit output value.
//
// On every call calculates another 8 bit random number and provides it as
// return value.
//
// See design issues [FuDsRNG], [FuDsRandomRepeatable], FuDsRngConstants,
// FuDsBitPeriods.
//

static uint8_t  lcg16_8 ()
{
    static const uint16_t  fact = 64747;
    static const uint16_t  addconst = 1;
    static       uint16_t  state;

    state *= fact;
    state += addconst;
    return (uint8_t)(state >> 4);
}



//==============================================================================
// memcpyRev
//
// Copy bytes from *src to *dest but reverse the order.
// The regions pointed-to by src and dest must not overlap.
//

static void memcpyRev (
    uint8_t  *const  dest,
    uint8_t  *const  src,
    const int16_t    n
) {
    int16_t  i;
    int16_t  j;
    for (i=0, j=n-1;  j>=0;  --j, ++i) {
        dest[i] = src[j];
    }
}



//==============================================================================
// ws2812out
//
// Regarding inline assembler syntax see the Inline Assembler Cookbook
// https://www.nongnu.org/avr-libc/user-manual/inline_asm.html
//
// Short summary:
// __asm__ (A:B:C:D)
//    A  Assembler instruction, operands are referenced by % followed by a digit
//    B  List of output operands, separated by commas
//    C  List of input operands, separated by commas
//    D  List of clobbered registers, separated by commas
// or
// __asm__ goto (A:B:C:D:Labels)
//    Labels  List of all C labels to which the assembler instruction may jump
//
// %= is used to create labels. %= is replaced by a unique number on every call.
// Pointer register Z is the combination of r30 and r31.
//

static void ws2812out () {
    __asm__ __volatile__ (
        "ldi r30, lo8(%0)\n\t"
        "ldi r31, hi8(%0)\n\t" : : "i"(&msg) : "r30","r31");
    cli();                          // Disable interrupts
#if 3*NLEDS > 0xFF
    ws2812out_OneLoop (0xFF);       // If more than 0xFF values transmit first 0xFF
#else
    ws2812out_OneLoop (3*NLEDS);    // Transmit rest
#endif

#if 3*NLEDS-0xFF > 0xFF
    ws2812out_OneLoop (0xFF);
#elif 3*NLEDS-0xFF > 0
    ws2812out_OneLoop (3*NLEDS - 0xFF);
#endif

#if 3*NLEDS-2*0xFF > 0xFF
    ws2812out_OneLoop (0xFF);
#elif 3*NLEDS-2*0xFF > 0
    ws2812out_OneLoop (3*NLEDS - 2*0xFF);
#endif

#if 3*NLEDS-3*0xFF > 0xFF
    ws2812out_OneLoop (0xFF);
#elif 3*NLEDS-3*0xFF > 0
    ws2812out_OneLoop (3*NLEDS - 3*0xFF);
#endif
    sei();                          // Enable interrupts again

    // Terminate transmission by WS2818 bus reset (>50 us)
    WAIT_US(55);
}



//==============================================================================
// Calculate the NOPs (no-operation statements) in a piece of assembler code to
// achieve the required WS2812B timing
//
// WS2812B specification on the timing to put out one bit:
//
// "0":_-----___________-   "1":_---------_____-
//      <T0H><---T0L--->         <--T1H--><T1L>
//           <T01L><T1L>
//
// T01L = T0L - T1L

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Nominal times for the periods of the WS2812B protocol
// See design document section "Selecting Nominal Protocol Values"

#define  T1H_NOM   1000  // Nominal time for T1H in ns
#define  T1L_NOM    380  // Nominal time for T1L in ns
#define  T0H_NOM    380  // Nominal time for T0H in ns
#define  T0L_NOM   1000  // Nominal time for T0L in ns

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Number of assembly instruction CPU cycles (beside NOPs)
// See design issue [SwDsOneBitOut]

#define  NC1H         5  // No. of asm instr. CPU cyles (beside NOPs) for T1H
#define  NC1L         3  // No. of asm instr. CPU cyles (beside NOPs) for T1L
#define  NC0H         6  // No. of asm instr. CPU cyles (beside NOPs) for T0H
#define  NC0L         1  // No. of asm instr. CPU cyles (beside NOPs) for T0L

//------------------------------------------------------------------------------
// Templates to place a certain number of NOPS in assembler code

#define  NOPS_0     ""
#define  NOPS_1     NOPS_0  "nop\n\t"
#define  NOPS_2     NOPS_1  "nop\n\t"
#define  NOPS_3     NOPS_2  "nop\n\t"
#define  NOPS_4     NOPS_3  "nop\n\t"
#define  NOPS_5     NOPS_4  "nop\n\t"
#define  NOPS_6     NOPS_5  "nop\n\t"
#define  NOPS_7     NOPS_6  "nop\n\t"
#define  NOPS_8     NOPS_7  "nop\n\t"
#define  NOPS_9     NOPS_8  "nop\n\t"
#define  NOPS_10    NOPS_9  "nop\n\t"
#define  NOPS_11    NOPS_10 "nop\n\t"
#define  NOPS_12    NOPS_11 "nop\n\t"
#define  NOPS_13    NOPS_12 "nop\n\t"
#define  NOPS_14    NOPS_13 "nop\n\t"
#define  NOPS_15    NOPS_14 "nop\n\t"
#define  NOPS_16    NOPS_15 "nop\n\t"
#define  NOPS_17    NOPS_16 "nop\n\t"
#define  NOPS_18    NOPS_17 "nop\n\t"
#define  NOPS_19    NOPS_18 "nop\n\t"
#define  NOPS_20    NOPS_19 "nop\n\t"
#define  NOPS_21    NOPS_20 "nop\n\t"
#define  NOPS_22    NOPS_21 "nop\n\t"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// At first calculate the number of NOPs for the short periods T1L and T0H

// Number of NOPS to realize T1L
#define  N_NOPS_T1L  ( ((T1L_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC1L) >=0 ?\
                       ((T1L_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC1L) : 0 )
// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T1L < 1
#  define  NOPS_T1L  NOPS_0
#elif    N_NOPS_T1L < 2
#  define  NOPS_T1L  NOPS_1
#elif    N_NOPS_T1L < 3
#  define  NOPS_T1L  NOPS_2
#elif    N_NOPS_T1L < 4
#  define  NOPS_T1L  NOPS_3
#elif    N_NOPS_T1L < 5
#  define  NOPS_T1L  NOPS_4
#elif    N_NOPS_T1L < 6
#  define  NOPS_T1L  NOPS_5
#elif    N_NOPS_T1L < 7
#  define  NOPS_T1L  NOPS_6
#elif    N_NOPS_T1L < 8
#  define  NOPS_T1L  NOPS_7
#elif    N_NOPS_T1L < 9
#  define  NOPS_T1L  NOPS_8
#elif    N_NOPS_T1L < 10
#  define  NOPS_T1L  NOPS_9
#else
#  error no valid NOPS_T1L
#endif

// Number of NOPS to realize T0H
#define  N_NOPS_T0H  ( ((T0H_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC0H) >=0 ?\
                       ((T0H_NOM)*(((F_CPU)+500)/1000)+500000)/1000000 - (NC0H) : 0 )
// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T0H < 1
#  define  NOPS_T0H  NOPS_0
#elif    N_NOPS_T0H < 2
#  define  NOPS_T0H  NOPS_1
#elif    N_NOPS_T0H < 3
#  define  NOPS_T0H  NOPS_2
#elif    N_NOPS_T0H < 4
#  define  NOPS_T0H  NOPS_3
#elif    N_NOPS_T0H < 5
#  define  NOPS_T0H  NOPS_4
#elif    N_NOPS_T0H < 6
#  define  NOPS_T0H  NOPS_5
#elif    N_NOPS_T0H < 7
#  define  NOPS_T0H  NOPS_6
#elif    N_NOPS_T0H < 8
#  define  NOPS_T0H  NOPS_7
#elif    N_NOPS_T0H < 9
#  define  NOPS_T0H  NOPS_8
#elif    N_NOPS_T0H < 10
#  define  NOPS_T0H  NOPS_9
#else
#  error no valid NOPS_T0H
#endif

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Calculate the number of NOPs of the long periods T0L and T1H

// Required number of total assembler instructions to achieve the required freq.
#define  N_INSTR_TOTAL  ((( (T0H_NOM)+(T0L_NOM) )*(((F_CPU)+500)/1000)+500000)/1000000)

// Number of NOPS to realize T1H
#define  N_NOPS_T1H     ((N_INSTR_TOTAL) - (N_NOPS_T1L) - (NC1L) - (NC1H))

// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T1H < 6
#  error no valid NOPS_T1H
#elif    N_NOPS_T1H < 7
#  define  NOPS_T1H  NOPS_6
#elif    N_NOPS_T1H < 8
#  define  NOPS_T1H  NOPS_7
#elif    N_NOPS_T1H < 9
#  define  NOPS_T1H  NOPS_8
#elif    N_NOPS_T1H < 10
#  define  NOPS_T1H  NOPS_9
#elif    N_NOPS_T1H < 11
#  define  NOPS_T1H  NOPS_10
#elif    N_NOPS_T1H < 12
#  define  NOPS_T1H  NOPS_11
#elif    N_NOPS_T1H < 13
#  define  NOPS_T1H  NOPS_12
#elif    N_NOPS_T1H < 14
#  define  NOPS_T1H  NOPS_13
#elif    N_NOPS_T1H < 15
#  define  NOPS_T1H  NOPS_14
#elif    N_NOPS_T1H < 16
#  define  NOPS_T1H  NOPS_15
#elif    N_NOPS_T1H < 17
#  define  NOPS_T1H  NOPS_16
#elif    N_NOPS_T1H < 18
#  define  NOPS_T1H  NOPS_17
#elif    N_NOPS_T1H < 19
#  define  NOPS_T1H  NOPS_18
#elif    N_NOPS_T1H < 20
#  define  NOPS_T1H  NOPS_19
#elif    N_NOPS_T1H < 21
#  define  NOPS_T1H  NOPS_20
#elif    N_NOPS_T1H < 22
#  define  NOPS_T1H  NOPS_21
#else
#  error no valid NOPS_T1H
#endif

// Number of NOPS to realize T01L=T0L-T1L
#define  N_NOPS_T01L    ((N_INSTR_TOTAL)-(N_NOPS_T0H)-(NC0H)-(NC0L)-(N_NOPS_T1L))

// Note: NOPS_X is part of a character string, therefore no parantheses allowed:
#if      N_NOPS_T01L < 1
#  define  NOPS_T01L  NOPS_0
#elif    N_NOPS_T01L < 2
#  define  NOPS_T01L  NOPS_1
#elif    N_NOPS_T01L < 3
#  define  NOPS_T01L  NOPS_2
#elif    N_NOPS_T01L < 4
#  define  NOPS_T01L  NOPS_3
#elif    N_NOPS_T01L < 5
#  define  NOPS_T01L  NOPS_4
#elif    N_NOPS_T01L < 6
#  define  NOPS_T01L  NOPS_5
#elif    N_NOPS_T01L < 7
#  define  NOPS_T01L  NOPS_6
#elif    N_NOPS_T01L < 8
#  define  NOPS_T01L  NOPS_7
#elif    N_NOPS_T01L < 9
#  define  NOPS_T01L  NOPS_8
#elif    N_NOPS_T01L < 10
#  define  NOPS_T01L  NOPS_9
#elif    N_NOPS_T01L < 11
#  define  NOPS_T01L  NOPS_10
#elif    N_NOPS_T01L < 12
#  define  NOPS_T01L  NOPS_11
#elif    N_NOPS_T01L < 13
#  define  NOPS_T01L  NOPS_12
#elif    N_NOPS_T01L < 14
#  define  NOPS_T01L  NOPS_13
#elif    N_NOPS_T01L < 15
#  define  NOPS_T01L  NOPS_14
#elif    N_NOPS_T01L < 16
#  define  NOPS_T01L  NOPS_15
#elif    N_NOPS_T01L < 17
#  define  NOPS_T01L  NOPS_16
#elif    N_NOPS_T01L < 18
#  define  NOPS_T01L  NOPS_17
#elif    N_NOPS_T01L < 19
#  define  NOPS_T01L  NOPS_18
#elif    N_NOPS_T01L < 20
#  define  NOPS_T01L  NOPS_19
#elif    N_NOPS_T01L < 21
#  define  NOPS_T01L  NOPS_20
#elif    N_NOPS_T01L < 22
#  define  NOPS_T01L  NOPS_21
#else
#  error no valid NOPS_T01L
#endif


//==============================================================================
// ws2812out_OneLoop
//
// Put out one slice of up to 256 bytes
//
// forceinline is a GCC extension
//

static forceinline void  ws2812out_OneLoop (
    const uint8_t  nCycles
) {
    // Put out one bit
    // The bit number is passed as argument bit
    //
    // The macro implements the following algorithm:
    //       Set bit 0 in PORTB
    //       Load r25 from *(Z=r30/r31)
    //       if bit "bit" of r25 is not set then jump tp L%=a
    //          Execute a number of nops for T1H
    //          Reset bit "bit" PORTB
    //          Jump to L%=B
    // L%=a: Execute a number of for T0H
    //          Reset bit "bit" PORTB
    //          Execute a number of nops for T01L=(T0L-T1L)
    // L%=b:
    //
    // maskHi and maskLo are the WS2812B port output bitmasks to set and reset
    // the WS2812B output pin and permanently tied to r3 and r4 at startup of
    // the program to be fast accessible by the out instructions.
    //
    // NOPS_T1L is not integrated into the macro because after the last macro
    // call the time is needed to calculate the loop termination.

#define ONE_BIT_OUT(bit) \
    __asm__ __volatile__ ( \
        "out %0,%2\n\t" \
        "ld r25,Z\n\t" \
        "sbrs r25,%1\n\t" \
        "rjmp L%=a\n\t" \
        NOPS_T1H \
        "out %0,%3\n\t" \
        "rjmp L%=b\n\t" \
        "L%=a:\n\t" \
        NOPS_T0H \
        "out %0,%3\n\t" \
        NOPS_T01L \
        "L%=b:\n\t" \
        : : "I"(_SFR_IO_ADDR(PORTB)),"I"(bit),"r"(maskHi),"r"(maskLo): "r24","r25","r30","r31")

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    // Initialize the loop counter register r24 with the no. of cycles nCycles
    __asm__ __volatile__ (
        "ldi r24, %0\n\t" : : "i"(nCycles) : "r24","r30","r31");

    // Loop through all bytes of the current slice
loop1:                                      // cppcheck-suppress unusedLabel
    // Put out each bit of the current byte in *Z (i.e. r30/31) one by one
    ONE_BIT_OUT(7); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(6); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(5); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(4); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(3); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(2); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(1); __asm__ __volatile__ (NOPS_T1L::);
    ONE_BIT_OUT(0);

    // Do the loop management:
    // - Increment the pointer register Z consisting of r30 and r31.
    // - Decrement the loop counter r24.
    // - Check whether the loop counter reached zero and
    // - jump back to the start of the loop if that's not the case.
    __asm__ __volatile__ goto (
        "adiw r30, 0x01\n\t"
        "subi r24, 0x01\n\t"
        "cpse r24, r1\n\t"
        "rjmp %l0\n\t" : : : "r24", "r30", "r31" : loop1);
}



//==============================================================================
// setTimers
//
// Initialize Timers
//

static void setTimers ()
{
    // Timer2 clock divider
    // NOTE: Change when changing the CS2x bits
    static const uint16_t  clkDivT2 = 1024;

    TIMSK0 = 0;                                 // Disable all Timer0 interrupts
    TIMSK1 = 0;                                 // Disable all Timer1 interrupts

    TCCR2A =  (1<<WGM21)|(1<<WGM20);            // Fast PWM mode, TOP = OCRA
                                                // OC0A,OC0B disconnected
    TCCR2B =  (1<<WGM22)                        // Fast PWM mode, TOP = OCRA
            | (1<<CS22)|(1<<CS21)|(1<<CS20);    // Clock divider 1:1024

    OCR2A = (uint8_t)(TIMEBASE*F_CPU/clkDivT2 +0.5);    // Timer2 TOP value

    TIMSK2 = (1<<OCIE2A);           // Enable Timer2 Compare Match A Interrupt
}



//==============================================================================
// Timer2 Compare Match A Interrupt Service Routine
//
// Increment system time counter
//

ISR(TIMER2_COMPA_vect)                      // cppcheck-suppress unusedFunction
{
    ++timeCnt;
}
