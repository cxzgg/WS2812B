//##############################################################################
//
// serial.cpp
//
// Functions to access the serial interface of an ATmega328P
//
// Version 2   2022 Sep 10th   jost.brachert@gmx.de
//                             CPP error directive regarding F_CPU value removed
//                             (error directive regarding F_CPU defined remains)
//
// Version 1   2017 Jan 14th   jost.brachert@gmx.de
//
// The program is intended to be run on an ATMEL ATmega328P as e.g. used in an
// Arduino Uno.
//
// Copyright (C) 2017-2022  Jost Brachert, jost.brachert@gmx.de
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the license, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
//

#include <stdint.h>          // (u)int8_t,(u)int16_t,(u)in32_t
#include <avr/io.h>          // UBRR0H/L,UCSR0A/B/C,UDR0,...
#include <avr/interrupt.h>   // sei(),cli()

#include "serial.h"


#define  RX_BUFFER_SIZE   16


struct RingBuffer {
    volatile uint8_t  head;
    volatile uint8_t  tail;
             uint8_t  buffer[RX_BUFFER_SIZE];
};

static RingBuffer  rxBuffer;


#define  TX_BUFFER_SIZE   16

static char  txBuffer[TX_BUFFER_SIZE];

static volatile uint8_t  transmit;       // Flag: =1: transmission in progress

static const char *volatile    txPtr;



//------------------------------------------------------------------------------
// serialSetup
//
// Set up serial interface
//

void  serialSetup () {

    cli();                              // Disable interrupts globally

    // See ATmega 328p Manual
    // ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET 11/2015
    // Atmel-8271J-AVR
    // 20.3 USART Clock Generation
#ifndef F_CPU
#error F_CPU not defined
#endif
    UBRR0H = 0;
                                        // U2X0=1: 8uL, U2X0=0: 16uL
    UBRR0L = (uint8_t)(static_cast<double>(F_CPU) / (8uL * 115200uL) - 1 + 0.5);

    UCSR0A = 1<<U2X0;                   // Halve the USART clock divider

    // Enable USART Receiver, Transmitter, RX Complete Interrupt
    // RXCIE0=1: RX 0 Complete Interrupt Enable
    // TXCIE0=0: No TX 0 Complete Interrupt
    // UDRIE0=0: USART 0 No Data Register Empty Interrupt Enable (not yet...)
    // RXEN0 =1: Receiver 0 Enable
    // TXEN0 =1: Transmitter 0 Enable
    // UCSZ02=0: see description at UCSR0C
    // RXB80 =0: Don't Receive Data bit 8 (the 9th bit) on RX 0
    // TXB80 =0: Don't Transmit Data bit 8 on TX 0
    UCSR0B |= (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0);

    // UMSEL01=0 UMSEL00=0: Asynchronous USART
    // UPM01=0 UPM00=0: No Parity
    // USBS0=0: 1 stop bit
    // USBS0=1: 2 stop bits
    // UCSZ02@UCSR0B=0 UCSZ01=1 UCSZ00=1:  Character Size 8 bit
    // UCPOL0=0: Clock Polarity (not relevant on asynchronous mode)
    UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);

    sei();                              // Enable interrupts globally again
}




//------------------------------------------------------------------------------
// Read one character from serial interface if there is one available

uint8_t  serialGetChar () {
    // If a new byte is received ...
    if (rxBuffer.head != rxBuffer.tail) {
        uint8_t  c = rxBuffer.buffer[rxBuffer.tail];
        ++rxBuffer.tail;
        if (rxBuffer.tail >= RX_BUFFER_SIZE)  rxBuffer.tail = 0;

        return c;
    }

    return 0;
}




//------------------------------------------------------------------------------
// Transmit a zero terminated character string on the serial interface

void  serialTransmit (
    const char * const  txString
) {
    while (transmit) {}     // Wait for transmit buffer getting ready so
                            //   that txPtr is ready to be assigned again

    txPtr = txString;
    transmit = 1;           // Initiate transmission
    UCSR0B |= 1<<UDRIE0;    // Enable transmitting interrupt
}




//------------------------------------------------------------------------------
// Transmit an integer value on the serial interface

void  serialTransmitValue (
    int32_t  value
) {
    uint32_t  uValue = value>=0 ? value : -value;

    while (transmit) {}     // Wait for transmit buffer getting ready so
                            //   that txPtr is ready to be assigned again

                            // txPtrTmp to be able to assign '-' to ptr target
    char  *txPtrTmp = itoa (uValue, TX_BUFFER_SIZE-2, txBuffer+1);
    if (value < 0)  *--txPtrTmp = '-';
    txPtr = txPtrTmp;

    txBuffer[TX_BUFFER_SIZE-1] = '\0';

    transmit = 1;           // Initiate transmission
    UCSR0B |= 1<<UDRIE0;    // Enable transmitting interrupt
}




//------------------------------------------------------------------------------
// itoa
//
// Convert a uint32_t fixed point value to a character string.
// Returns a pointer to the 1st digit.

char  *itoa (
    uint32_t  v,       // Input value
    uint8_t   n,       // Number of digits (w/o trailing '\0')
    char      s[]      // Pointer to string target, must be at least of size n+1
) {
    char  *p = s+n;
    *p = '\0';

    if ( ! v) {
        *--p = '0';
    } else {
        do {           // Insert digits while moving backward
            if (v) {
                *--p = '0' + v%10;
                v /= 10;
            } else {
                break;
            }
        } while (--n);
    }
    // Activate this if the passed pounter s is to be referenced in the calling
    // program:
#ifdef LEADINGBLANKS
    {   uint8_t  *p1 = p;
        while (--p1 >= s)  *p1 = ' ';
    }
#endif
    return p;
}




//------------------------------------------------------------------------------
// Receive Interrupt Service Routine

ISR(USART_RX_vect)                      // cppcheck-suppress unusedFunction
{
    rxBuffer.buffer[rxBuffer.head] = UDR0;
    ++rxBuffer.head;
    if (rxBuffer.head >= RX_BUFFER_SIZE)  rxBuffer.head = 0;
}




//------------------------------------------------------------------------------
// Data Register Empty Interrupt Service Routine

ISR(USART_UDRE_vect)                    // cppcheck-suppress unusedFunction
{
    if (transmit) {
        UDR0 = *txPtr;                  // Put data into buffer and send it
        ++txPtr;

        if (*txPtr == '\0') {
            // If all is sent disable the transmit interrupt
            UCSR0B &= ~(1<<UDRIE0);
            transmit = 0;               // Signal end of transmission to let
        }                               //   prepare for next transmission
    }
}

