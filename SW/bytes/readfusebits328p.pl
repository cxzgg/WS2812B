#!/usr/bin/perl -w
#
# readfusebits328p.pl
#
# Perl script to get the lock and fuse bits from the lock and fuse bytes of an
# ATmega328P processor.
#
# For usage info run the script with argument -h.
#
# Version 1.1   2020 Oct 16th   jost.brachert@gmx.de
#
# Copyright (C) 2017-2020  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#

use strict;

#==============================================================================
# Fuse Bytes Descriptors and Descriptions
#

#------------------------------------------------------------------------------
# LOCK BITS
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.1 Program And Data Memory Lock Bits

my @LB = (      # Lock Bits
    'Flash & EEPROM programming and verification disabled, boot bits & fuses locked',
    'Reserved',
    'Flash & EEPROM programming disabled, fuses locked',
    'No locks enabled',
);

my @BLB0 = (    # Boot Lock Bit Protection Mode 0
    'No appl programming by SPM instruction (Store Program Memory), no appl read by boot lock LPM',
    'No appl read by boot lock LPM instruction (Load Program Memory)',
    'No appl programming by SPM instruction (Store Program Memory)',
    'No appl programming restrictions',
);

my @BLB1 = (    # Boot Lock Bit Protection Mode 1
    'No boot loader programming by SPM instruction (Store Program Memory), no boot lock read by appl LPM',
    'No boot loader read by appl LPM instruction (Load Program Memory)',
    'No boot loader programming by SPM instruction (Store Program Memory)',
    'No boot loader programming restrictions',
);

my @locks = (
    {name=>'LB'    , bit=>0, size=>2, tab=>\@LB  , desc=>'Lock bits'},
    {name=>'BLB0'  , bit=>2, size=>2, tab=>\@BLB0, desc=>'Boot Lock 0 bits'},
    {name=>'BLB1'  , bit=>4, size=>2, tab=>\@BLB1, desc=>'Boot Lock 1 bits'},
    {name=>'lock-6', bit=>6, size=>1,            , desc=>'Reserved'},
    {name=>'lock-7', bit=>7, size=>1,            , desc=>'Reserved'},
);


#------------------------------------------------------------------------------
# LOW FUSE BYTES
#
# Clock Selection, Brown Out Detection, Start Up Time
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

sub cksel_sut {
    my $lfuse = $_[0];

    my $sut   = ($lfuse>>4) & 0x3;
    my $cksel = ($lfuse   ) & 0xF;

    my $ckseldesc = 'Select clock source';
    my $sutdesc   = 'Select start-up time';

    if ($cksel <= 0x3) {
        my @ckselval = (                               # CKSEL
            'External Clock',                          #  0000
            'Reserved',                                #  0001
            'Int. cal. RC Osc 8MHz',                   #  0010
            'Int. 128kHz Osc',                         #  0011
        );
        my @sutval = (                                         # SUT
            'BOD enabled',                                     #  00
            'Fast rising power',                               #  01
            'Slowly rising power',                             #  10
            'Reserved',                                        #  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL', $ckselval[$cksel], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'SUT'  , $sutval  [$sut  ], $sutdesc;
    } elsif ($cksel <= 0x5) {
        my @ckselval = (                               # CKSEL
            'LF Crystal Osc, SUT 1K CK',               #  0100
            'LF Crystal Osc, SUT 32K CK',              #  0101
        );
        my @sutval = (                                         # SUT
            'Add. 4CK       Fast rising power or BOD enabled', #  00
            'Add. 4CK+4.1ms Slowly rising power',              #  01
            'Add. 4CK+65ms  Stable frequency at start-up',     #  10
            'Reserved',                                        #  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL', $ckselval[$cksel-4], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'SUT'  , $sutval  [$sut    ], $sutdesc;
    } else {
        my @ckselval = (                               # CKSEL
            'Full Swing Crystal Osc',                  #  011x
            'Low Power Crystal Osc 0.4-0.9MHz',        #  100x
            'Low Power Crystal Osc 0.9-3.0MHz',        #  101x
            'Low Power Crystal Osc 3.0-8.0MHz',        #  110x
            'Low Power Crystal Osc 8.0-16.MHz',        #  111x
        );
        my @sutval = (                                 #CKSEL0 SUT
            'Ceramic resonator, fast rising power',    #     0  00
            'Ceramic resonator, slowly rising power',  #     0  01
            'Ceramic resonator, BOD enabled',          #     0  10
            'Ceramic resonator, fast rising power',    #     0  11
            'Ceramic resonator, slowly rising power',  #     1  00
            'Crystal Oscillator, BOD enabled',         #     1  01
            'Crystal Oscillator, fast rising power',   #     1  10
            'Crystal Oscillator, slowly rising power', #     1  11
        );
        printf "%-10s %-12s [%s]\n", 'CKSEL1-3', $ckselval[($cksel>>1)-3], $ckseldesc;
        printf "%-10s %-12s [%s]\n", 'CKSEL0/SUT', $sutval[(($cksel&1)<< 2)+$sut], $sutdesc;
    }
}

my @lfuse = (
    {name=>'CKOUT'  , bit=>6, size=>1, desc=>'Enable clock output'},
    {name=>'CKDIV8' , bit=>7, size=>1, desc=>'Divide clock by 8'},
);


#------------------------------------------------------------------------------
# HIGH FUSE BYTE
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

my @BOOTSZ = (
    '2048 words',
    '1024 words',
    ' 512 words',
    ' 256 words',
);

my @hfuse = (
    {name=>'BOOTRST' , bit=>0, size=>1,                desc=>'Select Reset Vector Address'},
    {name=>'BOOTSZ'  , bit=>1, size=>2, tab=>\@BOOTSZ, desc=>'Select Boot Size'},
    {name=>'EESAVE'  , bit=>3, size=>1,                desc=>'EEPROM memory is preserved through the Chip Erase'},
    {name=>'WDTON'   , bit=>4, size=>1,                desc=>'Watchdog Timer Always On'},
    {name=>'SPIEN'   , bit=>5, size=>1,                desc=>'Enable Serial Programming and Data Downloading'},
    {name=>'DWEN'    , bit=>6, size=>1,                desc=>'debugWIRE Enable'},
    {name=>'RSTDISBL', bit=>7, size=>1,                desc=>'External Reset Disable'},
);


#------------------------------------------------------------------------------
# EXTENDED FUSE BYTE
#
# Brown Out Detection
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

my @BODLEVEL = (
    'Reserved',         # 000
    'Reserved',         # 001
    'Reserved',         # 010
    'Reserved',         # 011
    'VBOTtyp 4.3V',     # 100
    'VBOTtyp 2.7V',     # 101
    'VBOTtyp 1.8V',     # 110
    'BOD disabled',     # 111
);

my @efuse = (
    {name=>'BODLEVEL', bit=>0, size=>3, tab=>\@BODLEVEL, desc=>'Brown-out detector trigger level'},
    {name=>'efuse-3' , bit=>3, size=>1,                , desc=>'Reserved'},
    {name=>'efuse-4' , bit=>4, size=>1,                , desc=>'Reserved'},
    {name=>'efuse-5' , bit=>5, size=>1,                , desc=>'Reserved'},
    {name=>'efuse-6' , bit=>6, size=>1,                , desc=>'Reserved'},
    {name=>'efuse-7' , bit=>7, size=>1,                , desc=>'Reserved'},
);



#==============================================================================
# Script
#

#------------------------------------------------------------------------------
# Script Arguments

my $dir = '.';
my $line;

if (defined $ARGV[0]) {
    if (substr($ARGV[0],0,1) eq '-') {
        print "usage: ".__FILE__." [directory]\n";
        print "The script reads the fuse and lock byte from files\n";
        print "locks.txt\nlfuse.txt\nhfuse.txt\nefuse.txt\n";
        exit (0);
    }
    $dir = $ARGV[0];
}


#------------------------------------------------------------------------------
# Analyze Fuse Bytes

readbits ("$dir/locks.txt", "Lock Byte", \@locks);
readbits ("$dir/lfuse.txt", "Low Fuse Byte", \@lfuse, \&cksel_sut);
readbits ("$dir/hfuse.txt", "High Fuse Byte", \@hfuse);
readbits ("$dir/efuse.txt", "Extended Fuse Byte", \@efuse);

print "Legend: '+': programmed (0), '-': not programmed (1)\n";
print "Note that the [comment] refers to the option, not to the current setting\n";





#==============================================================================
# Subroutines

sub readbits {
    my $file  = shift;
    my $label = shift;
    my $desc  = shift;
    my $sub   = shift;

    if ( ! open FILE,$file) {
        print "$label: Could not open $file:\n$!\n\n";
        return;
    }

    my $line = <FILE>;
    chomp $line;
    print "$label: $line\n";
    $line = oct $line;

    for my $h (@{$desc}) {
        my $v = ($line >> $h->{bit}) & ((1 << $h->{size}) - 1);
        if ($h->{size} == 1) {
            unless ($h->{desc} eq 'Reserved' and $v) {
                printf "%-10s %-12s [%s]\n", $h->{name}, $v?'-':'+', $h->{desc};
            }
        } else {
            printf "%-10s %-12s [%s]\n", $h->{name}, $h->{tab}->[$v], $h->{desc};
        }
    }

    &{$sub}($line)  if defined $sub;

    print "\n";
}
