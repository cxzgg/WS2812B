#!/usr/bin/perl -w
#
# createfusebits328p.pl
#
# Perl script to calculate the lock and fuse bytes for WS2812B Driving Unit
# on ATmega328P
#
# To use set the parameters in this source file as described in the comments
# below. Result is printed to STDOUT.
#
# Version 1.0   2017 Jan 14th   jost.brachert@gmx.de
#
# Copyright (C) 2017  Jost Brachert, jost.brachert@gmx.de
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the license, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program, see file COPYING. If not, see https://www.gnu.org/licenses/.
#

use strict;


my $i;

#==============================================================================
# LOCK BITS
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.1 Program And Data Memory Lock Bits

#------------------------------------------------------------------------------
# Lock Bit Settings
#
# Note: Lock bits can only be erased to 1 by Chip Erase command.
#

# lock 0-1  Lock Bits
#           Set exactly one of the elements to one, all others to zero
my @LB = (
    0,  # Flash & EEPROM programming and verification disabled, boot bits & fuses locked
    0,  # Reserved
    0,  # Flash & EEPROM programming disabled, fuses locked
    1,  # No locks enabled
);

# lock 2-3  Boot Lock Bit Protection Mode 0
#           Set exactly one of the elements to one, all others to zero
my @BLB0 = (
    0,  # No appl programming by SPM instruction (Store Program Memory),
        # no appl read by boot lock LPM
    0,  # No appl read by boot lock LPM instruction (Load Program Memory)
    0,  # No appl programming by SPM
    1,  # No appl programming restrictions
);

# lock 4-5  Boot Lock Bit Protection Mode 1
#           Set exactly one of the elements to one, all others to zero
my @BLB1 = (
    0,  # No boot loader programming by SPM instruction (Store Program Memory),
        # no boot loader read by appl LPM
    0,  # No boot loader read by appl LPM instruction (Load Program Memory)
    0,  # No boot loader programming by SPM
    1,  # No boot loader programming restrictions
);

# Set to 1 if to be programmed:
my $lock6 = 0;      # Reserved
my $lock7 = 0;      # Reserved

#------------------------------------------------------------------------------
# Lock Byte Assembly

    my $locks = 0;

    for ($i=0; $i<@LB; ++$i) {
        last if $LB[$i];
    }
    $locks |= $i;

    for ($i=0; $i<@BLB0; ++$i) {
        last if $BLB0[$i];
    }
    $locks |= $i << 2;

    for ($i=0; $i<@BLB1; ++$i) {
        last if $BLB1[$i];
    }
    $locks |= $i << 4;

    $locks |= ( ! $lock6) << 6;
    $locks |= ( ! $lock7) << 7;


#==============================================================================
# LOW FUSE BYTE
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

#------------------------------------------------------------------------------
# Low Fuse Byte Settings
#
#   Displayed bit values here: 0->programmed, 1->erased
#
# - - - - - - - - - - - - - - - - - - - - -   SUT CKSEL
# External Clock                                   0000
# Reserved                                         0001
# Internal Calibrated RC Oscillator                0010
# 128kHz Internal Oscillator                       0011
# Start Up Time (SUT) recommended for
#  BOD enabled                                 00
#  Fast rising power                           01
#  Slowly rising power                         10
#  Reserved                                    11
#
# - - - - - - - - - - - - - - - - - - - - -   SUT CKSEL
# Low-Frequency Crystal Oscillator                 010x
#  Start Up Time 1K CK                                0
#  Start Up Time 32K CK                               1
# Additional Delay (by Watchdog Oscillator)
#  4CK       Fast rising power or BOD enabled  00
#  4CK+4.1ms Slowly rising power               01
#  4CK+65ms  Stable frequency at start-up      10
#  Reserved                                    11
#
# - - - - - - - - - - - - - - - - - - - - -   SUT CKSEL
# Full Swing Crystal Oscillator                    011x
#
# Low Power Crystal Oscillator
#     0.4 -  0.9 MHz                               100x
#     0.9 -  3.0 MHz                               101x
#     3.0 -  8.0 MHz                               110x
#     8.0 - 16.0 MHz                               111x
#
# Low Power/Full Swing Crystal Oscillator:
# Ceramic resonator, fast rising power         00     0
# Ceramic resonator, slowly rising power       01     0
# Ceramic resonator, BOD enabled               10     0
# Ceramic resonator, fast rising power         11     0
# Ceramic resonator, slowly rising power       00     1
# Crystal Oscillator, BOD enabled              01     1
# Crystal Oscillator, fast rising power        10     1
# Crystal Oscillator, slowly rising power      11     1

# !! Set bit to 0 if to be programmed
my $SUT   =   0b10;
my $CKSEL = 0b0111;

# Set to 1 if to be programmed:
my $CKOUT  = 0;     # 1: Enable clock output
my $CKDIV8 = 0;     # 1: Divide clock by 8

#------------------------------------------------------------------------------
# Low Fuse Byte Assembly

    my $lfuse = ($SUT << 4) | $CKSEL;
    $lfuse |= ( ! $CKOUT ) << 6;
    $lfuse |= ( ! $CKDIV8) << 7;


#==============================================================================
# HIGH FUSE BYTE
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

#------------------------------------------------------------------------------
# High Fuse Byte Settings

my @BOOTSZ = (  # Set exactly one of the elements to one, all others to zero:
    0,  # 2048 words
    0,  # 1024 words
    0,  #  512 words
    1,  #  256 words
);

# Set to 1 if to be programmed:
my $BOOTRST = 0;    # Select Reset Vector Address
                    # 0: Select Address 0
                    # 1: Select start of boot flash section
my $EESAVE  = 0;    # 1: EEPROM memory is preserved through the Chip Erase
my $WDTON   = 0;    # 1: Watchdog Timer always on
my $SPIEN   = 1;    # 1: Enable Serial Program and Data Downloading
my $DWEN    = 0;    # 1: debugWIRE Enable
my $RSTDISBL= 0;    # 1: External Reset Disable

#------------------------------------------------------------------------------
# High Fuse Byte Assembly

    my $hfuse = 0;

    for ($i=0; $i<@BOOTSZ; ++$i) {
        last if $BOOTSZ[$i];
    }
    $hfuse |= $i << 1;

    $hfuse |= ( ! $BOOTRST  ) << 0;
    $hfuse |= ( ! $EESAVE   ) << 3;
    $hfuse |= ( ! $WDTON    ) << 4;
    $hfuse |= ( ! $SPIEN    ) << 5;
    $hfuse |= ( ! $DWEN     ) << 6;
    $hfuse |= ( ! $RSTDISBL ) << 7;


#==============================================================================
# EXTENDED FUSE BYTE
#
# ATMEL8-BITMICROCONTROLLER WITH 4/8/16/32KBYTES DATASHEET, Rev.8271J – 11/2015
#   http://www.atmel.com/devices/ATMEGA328.aspx
#   28.2 Fuse Bits

#------------------------------------------------------------------------------
# Extended Fuse Byte Settings

# Brownout Detection Trigger Level
my @BODLEVEL = (    # Set exactly one of the elements to one, all others to zero:
    0,  # Reserved           # 000 (0 here means programmed, 1 erased)
    0,  # Reserved           # 001
    0,  # Reserved           # 010
    0,  # Reserved           # 011
    0,  # VBOTtyp 4.3V       # 100  typical voltage 4.3 V
    0,  # VBOTtyp 2.7V       # 101  typical voltage 2.7 V
    0,  # VBOTtyp 1.8V       # 110  typical voltage 1.8 V
    1,  # BOD disabled       # 111
);

# Set to 1 if to be programmed:
my $efuse3 = 0;     # Reserved
my $efuse4 = 0;     # Reserved
my $efuse5 = 0;     # Reserved
my $efuse6 = 0;     # Reserved
my $efuse7 = 0;     # Reserved

#------------------------------------------------------------------------------
# Extended Fuse Byte Assembly

    my $efuse = 0;

    for ($i=0; $i<@BODLEVEL; ++$i) {
        last if $BODLEVEL[$i];
    }
    $efuse |= $i;

    $efuse |= ( ! $efuse3 ) << 3;
    $efuse |= ( ! $efuse4 ) << 4;
    $efuse |= ( ! $efuse5 ) << 5;
    $efuse |= ( ! $efuse6 ) << 6;
    $efuse |= ( ! $efuse7 ) << 7;


#==============================================================================
# OUTPUT

printf "locks:    %3d    0x%02x    0b%08b\n", $locks, $locks, $locks;
printf "lfuse:    %3d    0x%02x    0b%08b\n", $lfuse, $lfuse, $lfuse;
printf "hfuse:    %3d    0x%02x    0b%08b\n", $hfuse, $hfuse, $hfuse;
printf "efuse:    %3d    0x%02x    0b%08b\n", $efuse, $efuse, $efuse;
